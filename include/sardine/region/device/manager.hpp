#pragma once

#include <sardine/region/device.hpp>

#include <cuda/runtime_api.hpp>

#include <emu/cstring_view.hpp>
#include <emu/unordered_map.hpp>

#include <string>
#include <unordered_set>

namespace sardine::region::cuda
{

    struct manager : protected emu::unordered_map<std::string, handle>
    {
        using base = emu::unordered_map<std::string, handle>;

        static manager &instance();

    private:
        manager() = default;

        // This pool is used to keep the device memory alive.
        // There is no way to attach a device memory to a persistent file like shared memory.
        std::unordered_set<::cuda::memory::device::unique_ptr<std::byte[]>> pool;

    public:
        manager(const manager &) = delete;
        manager(manager &&) = delete;

        view_b open(std::string name);
        view_b create(std::string name, std::size_t size, ::cuda::device_t device);
        // open_or_create is not supported for device regions.
        // It is because it's not possible to know if the region was created or opened.
        // and so, it is not possible to know if the device memory was allocated or not.

        // Actully, maybe we can do it by trying to open the region, and if it fails, create it.
        // But it could produce data race.
        // view_b open_or_create(std::string name, std::size_t size, int device_id) = delete;

        view_b at(emu::cstring_view name);

        emu::optional<shm_handle> find_handle(const std::byte* ptr);

        void clear() {
            pool.clear();
        }

    };

} // namespace sardine::region::cuda
