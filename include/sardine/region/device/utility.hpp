#pragma once

#include <emu/cstring_view.hpp>
#include <emu/scoped.h>
#include <emu/cuda/span.h>
#include <emu/cuda/location.h>

#include <cuda/api/ipc.hpp>

#include <sardine/region/host.hpp>

namespace sardine::region::cuda
{

    using view_b = emu::cuda::span<std::byte>;

    struct handle_impl {
        cudaIpcMemHandle_t handle;
        std::size_t size;
        int device_id;
    };

    inline emu::cuda::location::device location(handle_impl h) {
        return {h.device_id};
    }

    inline std::byte* map_ptr(handle_impl handle) {
        std::byte* res;
        auto status = cudaIpcOpenMemHandle(reinterpret_cast<void**>(&res), handle.handle, cudaIpcMemLazyEnablePeerAccess);
        if (status != cudaSuccess) {
            throw std::runtime_error("Failed to open cuda ipc handle.");
        }
        return res;
    }

    inline auto map(handle_impl h) -> view_b {
        return {map_ptr(h), h.size};
    }

    struct handle {
        view_b data;
        bool owning = true;

        handle(handle_impl impl, bool owning)
            : data{map(impl)}
            , owning{owning}
        {}

        ~handle() {
            if (owning) {
                auto status = cudaIpcCloseMemHandle(data.data());
                if (status != cudaSuccess) {
                    fmt::print("Failed to close cuda ipc handle.");
                    std::terminate();
                }
            }
        }

    };

    struct shm_handle {
        emu::cstring_view name;
        std::size_t offset;
    };

} // namespace sardine::shm::region::cuda