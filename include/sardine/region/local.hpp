#pragma once

#include <sardine/type.hpp>
#include <sardine/type/url.hpp>

namespace sardin::local
{

namespace detail
{

    optional<url> url_of(span_cb data);

} // namespace detail

    constexpr auto url_scheme = "local";

template<typename T>
    optional<url> url_of(const T& value) {
        return detail::url_of(as_span(value)).map([&](auto url) {
            sardine::update_url(url, value);
            return url;
        });
    }

} // namespace sardin::local
