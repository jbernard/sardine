#pragma once

#include <sardine/region/device/utility.hpp>
#include <sardine/type/url.hpp>

#include <emu/optional.h>

namespace sardine::region::cuda
{

    view open(std::string name);

    view create(std::string name, std::size_t size, ::cuda::device_t device);

    view open_or_create(std::string name, std::size_t size, ::cuda::device_t device);

    emu::optional<shm_handle> find_handle(const std::byte* ptr);

namespace detail
{

    emu::optional<url> url_of(emu::cuda::span<const std::byte> data);

    view from_url(url_view url);

} // namespace detail

    template<typename T>
    inline emu::optional<url> url_of(emu::cuda::span<T> data) {
        return detail::url_of(emu::as_bytes(data));
    }

    template<emu::cpts::device_span Span>
    inline auto from_url(url_view url) -> Span {
        auto s = detail::from_url(url);
        // TODO: check size is multiple of sizeof(T)
        auto size = s.size() / sizeof(typename Span::value_type);
        return Span{ reinterpret_cast<typename Span::element_type*>(s.data()), size };
    }

    constexpr auto url_scheme = "device";

} // namespace sardine::region::cuda
