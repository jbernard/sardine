#pragma once

#include <sardine/type.hpp>
#include <sardine/region/host/utility.hpp>

#include <emu/cstring_view.hpp>
#include <emu/unordered_map.hpp>

#include <string>

namespace sardine::region::host
{

    struct manager : protected emu::unordered_map<std::string, handle>
    {
        using base = emu::unordered_map<std::string, handle>;

        static manager &instance();

    private:
        manager() = default;

    public:
        manager(const manager &) = delete;
        manager(manager &&) = delete;

        span_b open(std::string name);
        span_b create(std::string name, std::size_t size);
        span_b open_or_create(std::string name, std::size_t size);

        span_b at(emu::cstring_view name);

        optional<memory_handle> find_handle(const std::byte* ptr);
    };

} // namespace sardine::region::host
