#pragma once

#include <sardine/type.hpp>
#include <sardine/accessor.hpp>
#include <sardine/type/url.hpp>
#include <sardine/region/host/utility.hpp>

namespace sardine::region::host
{

    span_b open(std::string name);

    span_b create(std::string name, std::size_t size);

    span_b open_or_create(std::string name, std::size_t size);

    optional<memory_handle> find_handle(const std::byte* ptr);

    template<typename T>
    concept can_handle_type = std::is_trivial_v<T>;

    constexpr auto url_scheme = "host";

    optional<url> url_of_bytes(span_cb data);

    result<span_b> bytes_from_url(url_view u);

} // namespace sardine::region::host
