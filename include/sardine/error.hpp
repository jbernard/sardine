#pragma once

#include <emu/expected.hpp>

#include <system_error>

namespace sardine
{

    struct error_category: public std::error_category
    {
        const char * name() const noexcept;
        std::string message( int ev ) const;
    };

    enum class error
    {
        success = 0,

        url_resource_not_registered,
        url_unknown_scheme,

        accessor_rank_mismatch,
        accessor_not_scalar,
        accessor_not_range,
        accessor_not_contiguous,

        host_type_not_supported,
        host_url_invalid_path,
        host_url_offset_overflow,
        host_url_size_overflow,

        ring_missing_size,
        ring_url_missing_size,
        ring_url_missing_data,
        ring_url_missing_index,
        ring_url_missing_buffer_nb,
        ring_url_missing_policy,
        ring_url_invalid_policy
    };

    std::error_category const& sardine_category();

    /**
     * @brief Return a std::error_code from a sardine::error
     *
     * @param e
     * @return std::error_code
     */
    inline std::error_code make_ec( error e ) {
        return std::error_code( static_cast<int>( e ), sardine_category() );
    }

    /**
     * @brief Utility function to easily exit from a function that return a expected type.
     *
     * @param e
     * @return tl::unexpected< std::error_code >
     */
    inline emu::unexpected< std::error_code > make_unexpected( error e ) {
        return emu::unexpected( make_ec( e ) );
    }


} // namespace sardine

template<> struct std::is_error_code_enum< ::sardine::error >: std::true_type { };
