#pragma once

#include <sardine/type.hpp>
#include <sardine/error.hpp>

#include <sardine/region/host.hpp>

namespace sardine
{

namespace detail
{

        inline optional< result<span_b> > bytes_from_url( url_view u ) {
            auto scheme = u.scheme();

            if ( scheme == region::host::url_scheme )
                return region::host::bytes_from_url( u );

            return nullopt;
        }

        inline optional<url> url_of_bytes( span_cb bytes ) {
        EMU_RETURN_OPT_IF_TRUE(region::host::url_of_bytes(bytes));

        return nullopt;
    }


} // namespace detail

    inline result<span_b> bytes_from_url( url_view u ) {
        EMU_RETURN_OPT_IF_TRUE(detail::bytes_from_url(u));

        return make_unexpected(error::url_unknown_scheme);
    }

    template<typename T>
    result<T> from_url( url_view u ) {
        auto scheme = u.scheme();

        if (auto view = bytes_from_url(u); view)
            return make_accessor<T>(u.params())
                .map([&](auto as) { return as.convert(*view); });

        return make_unexpected( error::url_unknown_scheme );
    }

    // Special case when explicitly asks for a url
    // we return what we have regardless of if there is a url or not.
    template<>
    inline result<url> from_url<url>( url_view u ) {
        return url{u};
    }

    inline result<url> url_of_bytes( span_cb bytes ) {
        EMU_RETURN_OPT_IF_TRUE(detail::url_of_bytes(bytes));

        return make_unexpected( error::url_resource_not_registered );
    }

    template<typename T>
    result<url> url_of(const T& value) {
        if constexpr ( cpts::url_aware<T> )
            return value.url();
        else {
            auto view = as_span_of_bytes(value);

            if ( auto url = url_of_bytes( view ); url ) {
                sardine::update_url( *url, value);
                return *url;
            }
        }

        return make_unexpected( error::url_resource_not_registered );
    }

} // namespace sardine
