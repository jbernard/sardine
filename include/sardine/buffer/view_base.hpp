#pragma once

#include <sardine/concepts.hpp>
#include <sardine/accessor.hpp>

namespace sardine::buffer
{

    template <typename T, typename Derived>
    struct view_base : sardine::accessor<T>
    {
        using accessor_t = sardine::accessor<T>;

        view_base(const accessor_t & a) : accessor_t(a) {}
        view_base(const view_base &) = default;

        sardine::accessor<T>       & accessor()       { return *this; }
        sardine::accessor<T> const & accessor() const { return *this; }

    private:
        // Derived& self() { return *static_cast<Derived *>(this); }
        const Derived& self() const { return *static_cast<const Derived *>(this); }
    };

    template <cpts::view V, typename Derived>
    struct view_base< V, Derived > : sardine::accessor< V >
    {
        using accessor_t = sardine::accessor< V >;

        using element_type = typename V::element_type;

        view_base(const accessor_t & a) : accessor_t(a) {}
        view_base(const view_base &) = default;

        accessor_t       & accessor()       { return *this; }
        accessor_t const & accessor() const { return *this; }

        auto close_lead_dim() const
        {
            using new_type = view_base<element_type, Derived>;
            using new_accessor_t = typename new_type::accessor_t;
            return new_type{new_accessor_t{this->offset}};
        }

        auto subspan(std::size_t new_offset, std::size_t new_size = std::dynamic_extent) const {
            return self().clone_with_new_accessor(accessor_t::subspan(new_offset, new_size));
        }

    private:
        // Derived &self() { return *static_cast<Derived *>(this); }
        const Derived &self() const { return *static_cast<const Derived *>(this); }
    };

    template <cpts::mdview V, typename Derived>
    struct view_base< V, Derived > : sardine::accessor< V >
    {
        using accessor_t = sardine::accessor< V >;

        using element_type = typename V::element_type;

        view_base(const accessor_t & a) : accessor_t(a) {}
        view_base(const view_base &) = default;

        accessor_t       & accessor()       { return *this; }
        accessor_t const & accessor() const { return *this; }

        template<typename = void> // lazy compilation to avoid recursive type instantiation
        auto close_lead_dim() const requires (V::rank() > 0) {
            return submdspan(0);
        }

        template<class... SliceSpecifiers>
        auto submdspan(SliceSpecifiers... specs) const {
            return self().clone_with_new_accessor(this->submdspan_accessor(specs...));
        }

    private:
        const Derived &self() const { return *static_cast<const Derived *>(this); }
    };

} // namespace sardine::buffer
