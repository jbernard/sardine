#pragma once

#include <sardine/context.hpp>
#include <sardine/buffer/base.hpp>
#include <sardine/buffer/adaptor.hpp>

namespace sardine::ring
{

    constexpr auto url_scheme = "ring";

    constexpr auto data_key       = "r_data";
    constexpr auto index_key      = "r_index";
    constexpr auto offset_key     = "r_offset";
    constexpr auto size_key       = "r_size";
    constexpr auto stride_key     = "r_stride";
    constexpr auto buffer_nb_key  = "r_buffer_nb";
    constexpr auto policy_key     = "r_policy";

// namespace detail
// {

//     template<typename T>
//     struct reduce_view;

//     template<cpts::view V>
//     struct reduce_view<V> {
//         using type = typename V::value_type;
//     };

//     template<cpts::mdview V>
//     struct reduce_view<V> {
//         using type = decltype(emu::submdspan(std::declval<V>(), 0));
//     };

// } // namespace detail

//     template<typename T>
//     using reduce_type = typename detail::reduce_view<T>::type;

    enum class next_policy {
        last, next
    };

    inline auto format_as(next_policy p) {
        switch (p) {
            case next_policy::last : return "last";
            case next_policy::next : return "next";
        }
        EMU_UNREACHABLE;
    }

    inline result< next_policy > parse_policy( std::string_view s) {
        if (s == "last") return next_policy::last;
        if (s == "next") return next_policy::next;

        return make_unexpected( error::ring_url_invalid_policy );
    }

    struct index
    {

        box<std::size_t, host_ctx> global_index; // move to 2 values to be able to detect full vs empty.
        std::size_t idx;
        std::size_t buffer_nb;
        next_policy policy;

        std::size_t save_index = {};

        index(box<std::size_t, host_ctx> global_index, std::size_t buffer_nb, next_policy policy);
        index(std::size_t& global_index, std::size_t buffer_nb, next_policy policy);

        void incr_local();
        void decr_local();

        // check if the next index is available
        bool has_next(host_ctx& ctx);

        void send(host_ctx& ctx);
        void recv(host_ctx& ctx);

        void revert_send(host_ctx& ctx);
        void revert_recv(host_ctx& ctx);

        sardine::url url() const {
            sardine::url u = global_index.url();

            auto param_ref = u.params();

            param_ref.set(buffer_nb_key, fmt::to_string(buffer_nb));
            param_ref.set(policy_key, fmt::to_string(policy));

            return u;
        }

        static result<index> open(sardine::url url) {
            auto params = url.params();

            auto buffer_nb = urls::try_parse_at<std::size_t>(params, buffer_nb_key);
            EMU_TRUE_OR_RETURN_UNEXPECTED(buffer_nb, make_ec(error::ring_url_missing_buffer_nb));

            auto opt_policy = urls::try_get_at(params, policy_key);
            EMU_TRUE_OR_RETURN_UNEXPECTED(opt_policy, make_ec(error::ring_url_missing_policy));
            auto policy = parse_policy(*std::move(opt_policy));
            EMU_TRUE_OR_RETURN_ERROR(policy);

            return box<std::size_t, host_ctx>::open(url).map([&](auto global_index) {
                return index(global_index, *buffer_nb, *policy);
            });
        }


        // static result<index> open(sardine::url url, std::size_t buffer_nb, next_policy policy);

        // template<typename Idx>
        // result<index> create(Idx&& idx, std::size_t buffer_nb, next_policy policy = next_policy::last) {
        //     auto url = url_of(std::forward<Idx>(idx));
        //     if (not url)
        //         return unexpected(url.error());

        //     auto b = box<std::size_t>::open(*url);
        //     if (not b)
        //         return unexpected(b.error());

        //     return index(*std::move(b), buffer_nb, policy);
        // }

    };


    struct view_t {
        sardine::url data_url;
        span_b data;
        index idx;
        std::size_t size;   // size of the buffer
        std::size_t offset; // distance from the start of the buffer
        std::size_t stride; // distance between buffers
        std::size_t element_size;

        view_t(sardine::url data_url, span_b data, index idx, std::size_t size, std::size_t offset, std::size_t stride)
            : data_url(std::move(data_url))
            , data(std::move(data))
            , idx(std::move(idx))
            , size(size)
            , offset(offset)
            , stride((stride == std::dynamic_extent) ? size : stride)
        {}

        view_t(const view_t&) = default;

        // view_t(interface_t data, index idx, std::size_t size, std::size_t offset = 0, std::size_t stride = std::dynamic_extent)
        //     : view_t(sardine::view_t<T>(std::move(data)), std::move(idx), size, offset, stride)
        // {}

        span_b view() {
            return data.subspan(idx.idx * stride + offset, size);
        }

        sardine::url url() const {
            sardine::url url(fmt::format("{}://", url_scheme));
            auto param_ref = url.params();

            fmt::print("data url: {}\n", data_url);

            param_ref.set(data_key  , data_url.c_str());
            param_ref.set(index_key , idx.url().c_str());
            param_ref.set(offset_key, fmt::to_string(offset));
            param_ref.set(size_key  , fmt::to_string(size));
            param_ref.set(stride_key, fmt::to_string(stride));

            return url;
        }

        // static result< view_t > create(span_b data, index idx, std::size_t size, std::size_t offset = 0, std::size_t stride = std::dynamic_extent) {
        //     return url_of_bytes(data).map([&](auto url) {
        //         return view_t(url, std::move(data), std::move(idx), size, offset, stride);
        //     });
        // }
    };

    template<typename Ctx>
    struct producer : view_t
    {

        using view_t::view_t;

        producer(view_t view)
            : view_t(std::move(view))
        {}

        void send(Ctx& ctx) {
            idx.send(ctx);
        }

        void revert(Ctx& ctx) {
            idx.revert_send(ctx);
        }

    };

    template<typename Ctx>
    struct consumer : view_t
    {

        using view_t::view_t;

        consumer(view_t view)
            : view_t(std::move(view))
        {}

        void recv(Ctx& ctx) {
            idx.recv(ctx);
        }

        void revert(Ctx& ctx) {
            idx.revert_recv(ctx);
        }

    };


    template<typename Ctx>
    struct factory : sardine::buffer::interface::factory<Ctx>
    {
        md_descriptor_impl md_descriptor;
        view_t view;
        std::size_t offset_; // should I keep the offset ?

        factory(md_descriptor_impl md_descriptor, url_view u, span_b shm_data, index idx, std::size_t size, std::size_t offset, std::size_t stride)
            : md_descriptor(std::move(md_descriptor))
            , view(u, shm_data, std::move(idx), size, offset, stride)
        {}

        std::size_t offset() const override { return md_descriptor.offset(); }
        std::span<const std::size_t> extents() const override { return md_descriptor.extents(); }
        bool is_strided() const override { return md_descriptor.is_strided(); }
        std::span<const std::size_t> strides() const override { return md_descriptor.strides(); }

        result< s_producer<Ctx> > create_producer() override {
            return buffer::make_s_producer<Ctx>(producer<Ctx>{view});
        }

        result< s_consumer<Ctx> > create_consumer() override {
            return buffer::make_s_consumer<Ctx>(consumer<Ctx>{view});
        }

        static result< s_factory<Ctx> > create(url_view u) {
            auto params = u.params();

            auto size   = urls::try_parse_at<std::size_t>(params, size_key);
            EMU_TRUE_OR_RETURN_UNEXPECTED(size, make_ec(error::ring_url_missing_size));

            auto offset = urls::try_parse_at<std::size_t>(params, offset_key).value_or(0);
            auto stride = urls::try_parse_at<std::size_t>(params, stride_key).value_or(std::dynamic_extent);

            auto data    = urls::try_get_at(params, data_key);
            EMU_TRUE_OR_RETURN_UNEXPECTED(data, make_ec(error::ring_url_missing_data));
            auto url_idx = urls::try_get_at(params, index_key);
            EMU_TRUE_OR_RETURN_UNEXPECTED(url_idx, make_ec(error::ring_url_missing_index));

            auto memory_h = bytes_from_url(url(*data));
            EMU_TRUE_OR_RETURN_ERROR(memory_h);

            auto md = make_md_descriptor(params);
            EMU_TRUE_OR_RETURN_ERROR(md);

            return index::open(url(*url_idx)).map([&](auto idx) {
                return std::make_shared<factory>(*std::move(md), u, *memory_h, std::move(idx), *size, offset, stride);
            });

        }
    };


namespace detail
{

    template<typename T>
        requires cpts::closable_lead_dim< sardine::accessor< emu::decay<T> > >
    auto make_view(T&& data, index idx, std::size_t offset = 0)
    {
        auto accessor = from_view(EMU_FWD(data));

        auto view = as_span_of_bytes(data);

        auto closed_accessor = accessor.close_lead_dim();

        offset += closed_accessor.offset();

        constexpr static std::size_t t_size = sizeof(typename decltype(closed_accessor)::value_type);

        return url_of_bytes(view).map([&](auto url) {
            return std::make_pair(
                std::move(closed_accessor),
                view_t(
                    std::move(url), view,
                    std::move(idx),
                    closed_accessor.size() * t_size,
                    offset * t_size,
                    closed_accessor.lead_stride() * t_size
                )
            );
        });

    }

} // namespace detail

    template<typename T>
    auto make_view(T&& data, index idx, std::size_t offset = 0) {
        return detail::make_view(EMU_FWD(data), std::move(idx), offset).map([&](auto pair) {
            auto [accessor, view] = std::move(pair);
            using data_view_t = typename decltype(accessor)::view_type;
            return sardine::view_t<data_view_t>(accessor, buffer::make_s_view(std::move(view)));
        });
    }


    template<typename Ctx, typename T>
    auto make_producer(T&& data, index idx, std::size_t offset = 0) {
        return detail::make_view(EMU_FWD(data), std::move(idx), offset).map([&](auto pair) {
            auto [accessor, view] = std::move(pair);
            using data_view_t = typename decltype(accessor)::view_type;
            return sardine::producer<data_view_t, Ctx>(accessor, buffer::make_s_producer<Ctx>(producer<Ctx>(std::move(view))));
        });
    }

    template<typename Ctx, typename T>
    auto make_consumer(T&& data, index idx, std::size_t offset = 0) {
        return detail::make_view(EMU_FWD(data), std::move(idx), offset).map([&](auto pair) {
            auto [accessor, view] = std::move(pair);
            using data_view_t = typename decltype(accessor)::view_type;
            return sardine::consumer<data_view_t, Ctx>(accessor, buffer::make_s_consumer<Ctx>(consumer<Ctx>(std::move(view))));
        });
    }

} // namespace sardine::ring
