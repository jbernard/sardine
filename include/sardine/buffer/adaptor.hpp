#pragma once

#include <sardine/buffer/base.hpp>
#include <sardine/type/url.hpp>
#include <sardine/url.hpp>

#include <emu/mdspan.hpp>
#include <emu/info.hpp>

#include <fmt/format.h>

namespace sardine::buffer
{

namespace native
{

    struct view_t {
        span_b view_;
        span_b       view  (      ) { return view_; }

        sardine::url url() {
            auto maybe_url = url_of_bytes(view_);
            if (maybe_url)
                return *maybe_url;
            else
                throw std::runtime_error(fmt::format("url_of_bytes failed: {}", maybe_url.error()));
        }
    };

    template <typename Ctx>
    struct producer : view_t {

        void send  ( Ctx& ) { }
        void revert( Ctx& ) { }
    };

    template <typename Ctx>
    struct consumer : view_t {

        void recv  ( Ctx& ) { }
        void revert( Ctx& ) { }
    };

} // namespace native

namespace adaptor
{
    template<typename Impl>
    struct view_t : interface::view_t
    {
        Impl impl;

        view_t(Impl impl)
            : impl(std::move(impl))
        {}

        span_b view() override { return impl.view(); }
        sardine::url url() override { return impl.url(); }
    };

    template <typename Ctx, typename Impl>
    struct producer : interface::producer<Ctx>
    {
        Impl impl;

        producer(Impl impl)
            : impl(std::move(impl))
        {}

        void         send  (Ctx& ctx) override { impl.send(ctx);     }
        void         revert(Ctx& ctx) override { impl.revert(ctx);   }
        span_b       view  (        ) override { return impl.view(); }
        sardine::url url( ) override { return impl.url();  }
    };

    template <typename Ctx, typename Impl>
    struct consumer : interface::consumer<Ctx>
    {
        Impl impl;

        consumer(Impl impl)
            : impl(std::move(impl))
        {}

        void         recv  (Ctx& ctx) override { impl.recv(ctx);     }
        void         revert(Ctx& ctx) override { impl.revert(ctx);   }
        span_b       view  (        ) override { return impl.view(); }
        sardine::url url( ) override { return impl.url();  }
    };

} // namespace adaptor

    template <typename Impl>
    s_view make_s_view(Impl impl) {
        return std::make_shared< adaptor::view_t<Impl> >(std::move(impl));
    }

    template <typename Ctx, typename Impl>
    s_producer< Ctx > make_s_producer(Impl impl) {
        return std::make_shared< adaptor::producer<Ctx, Impl> >(std::move(impl));
    }

    template <typename Ctx, typename Impl>
    s_consumer< Ctx > make_s_consumer(Impl impl) {
        return std::make_shared< adaptor::consumer<Ctx, Impl> >(std::move(impl));
    }

    // template<typename Ctx, typename Impl>
    // struct circular_producer
    // {

    //     circular_producer(Impl impl, map_t parameter, std::size_t element_size)
    //         : impl  (std::move(impl))
    //         , idx (parameter, element_size)
    //     {
    //         // Set the index to the next position for writing.
    //         idx.incr_local();
    //     }

    //     Impl impl;
    //     index idx;

    //     span_b view() {
    //         return  idx.subspan(impl.view());
    //     }

    //     void send(Ctx& ctx) {
    //         // order does not matter here.
    //         idx.send();
    //         impl.send(ctx);
    //     }

    //     void revert() {
    //         idx.revert_send();
    //     }

    //     url_view url() {
    //         return impl.url();
    //     }

    // };

    // template<typename Ctx, typename Impl>
    // struct circular_consumer
    // {

    //     circular_consumer(Impl impl, map_t parameter, std::size_t element_size)
    //         : impl  (std::move(impl))
    //         , idx (parameter, element_size)
    //     {}

    //     Impl impl;
    //     index idx;

    //     span_b view() {
    //         return idx.subspan(impl.view());
    //     }

    //     void recv(Ctx& ctx) {
    //         // order does not matter here.
    //         idx.recv();
    //         impl.recv(ctx);
    //     }

    //     void revert() {
    //         idx.revert_recv();
    //     }

    //     url_view url() {
    //         return impl.url();
    //     }

    // };

} // namespace sardine::buffer

// JSONCONS_ENUM_TRAITS(sardine::buffer::index::next_policy, last, next)
