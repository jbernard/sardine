#pragma once

#include <sardine/type.hpp>
#include <sardine/url.hpp>
#include <sardine/context.hpp>
#include <sardine/accessor.hpp>
#include <sardine/buffer/view_base.hpp>

#include <emu/concepts.hpp>

#include <vector>
#include <memory>
#include <cstddef>

namespace sardine
{

    /**
     * @brief Additional parameter besides the url.
     *
     * It's based on sardine::json_t
     *
     */
    // using map_t = json_t;

namespace buffer
{

namespace detail
{

    template <typename T>
    struct adaptor_type {
        using value_type = T;
        // using proxy_type = T;
        using interface_type = T&;
    };

    template <cpts::all_view V>
    struct adaptor_type< V > {
        using value_type = typename V::value_type;
        // using proxy_type = std::span<value_type>;
        using interface_type = V;
    };

} // namespace detail

    /// The underlying type
    template<typename T>
    using value_type = typename detail::adaptor_type<T>::value_type;

    /// internal representation type for typed implementation.
    // template<typename T>
    // using proxy_type = typename detail::adaptor_type<T>::proxy_type;

    /// Type returned by producer and consumer.
    template<typename T>
    using interface_type = typename detail::adaptor_type<T>::interface_type;

namespace interface
{

    struct view_t
    {
        virtual ~view_t() = default;

        virtual span_b view() = 0;
        virtual sardine::url url() = 0;
    };

    template <typename Ctx>
    struct producer : view_t
    {
        virtual ~producer() = default;

        virtual void   send(Ctx&) = 0;
        virtual void   revert(Ctx&) = 0;

        // virtual sardine::url url() = 0;
    };

    template <typename Ctx>
    struct consumer : view_t
    {
        virtual ~consumer() = default;

        virtual void   recv(Ctx&) = 0;
        virtual void   revert(Ctx&) = 0;

        // virtual sardine::url url() = 0;
    };

} // namespace interface

} // namespace buffer

    using s_view = std::shared_ptr< buffer::interface::view_t >;

    template<typename Ctx>
    using s_producer = std::shared_ptr< buffer::interface::producer<Ctx> >;

    template<typename Ctx>
    using s_consumer = std::shared_ptr< buffer::interface::consumer<Ctx> >;

namespace buffer::interface
{

    template<typename Ctx>
    struct factory : sardine::interface::md_descriptor
    {
        virtual ~factory() = default;

        //Note: factory cannot guarantee that the producer and consumer are always available.
        // Some factories may be read-only, some may be write-only (unlikely).

        virtual result< s_producer<Ctx> > create_producer() = 0;
        virtual result< s_consumer<Ctx> > create_consumer() = 0;
    };

} // namespace buffer::interface

    template<typename Ctx>
    using s_factory = std::shared_ptr< buffer::interface::factory<Ctx> >;

    template<typename T>
    struct storage
    {
        // Note: empty storage since T is not a view.
        storage(span_b shm_data) {}

        // span_b data() {
        //     return std::as_writable_bytes(std::span{&value, 1});
        // }

        T init(T& shm_data) {
            return shm_data;
        }
    };

    template<cpts::view V>
    struct storage< V >
    {
        //TODO: use unique_ptr instead of vector.
        std::vector< std::byte > local_data;

        storage(span_b shm_data):
            local_data(shm_data.begin(), shm_data.end())
        {}

        // span_b data() {
        //     return local_data;
        // }

        V init(V shm_data) {
            using element_type = typename V::element_type;

            auto ptr = reinterpret_cast<element_type*>(local_data.data());

            if constexpr (cpts::mdview<V>)
                return V(ptr, shm_data.mapping);
            else
                return V(ptr, shm_data.size());
        }
    };

    template<typename T, typename Ctx>
    struct box : buffer::view_base< T, box< T, Ctx > >
    {
        using base_t = buffer::view_base< T, box >;
        using base_t::accessor_t;
        using base_t::accessor;

        using interface_t = buffer::interface_type<T>;

        s_producer<Ctx> prod_impl;
        s_consumer<Ctx> cons_impl;

        // storage may be empty if the T is not a view.
        [[no_unique_address]] sardine::storage<T> storage;
        T value;

        box(interface_t view);

        box( sardine::accessor<T> a, s_producer<Ctx> p_impl, s_consumer<Ctx> c_impl )
            : base_t(std::move(a))
            , prod_impl(std::move(p_impl))
            , cons_impl(std::move(c_impl))
            , storage(cons_impl->view())
            , value(storage.init(this->convert(cons_impl->view())))
        {}

        void send(Ctx& ctx) {
            this->copy(value, this->convert(prod_impl->view()));
            prod_impl->send(ctx);
        }
        void recv(Ctx& ctx) {
            cons_impl->recv(ctx);
            this->copy(this->convert(cons_impl->view()), value);
        }

        void revert(Ctx& ctx) {
            prod_impl->revert(ctx);
            cons_impl->revert(ctx);
        }

        sardine::url url() const {
            auto u = cons_impl->url();
            this->update_url(u);
            return u;
        }

        template<typename NT>
        auto clone_with_new_accessor(sardine::accessor<NT> new_accessor) const -> box<NT, Ctx> {
            return box<NT, Ctx>( new_accessor, prod_impl, cons_impl );
        }

        // static auto load(url_view u/* , map_t parameter = map_t() */) -> result<box>;
        static auto open(url_view u) -> result<box>;
    };

    template<typename T>
    struct view_t : buffer::view_base < T, view_t <T> >
    {
        using base_t = buffer::view_base< T, view_t >;
        using base_t::accessor_t;

        using interface_t = buffer::interface_type<T>;

        s_view impl;

        view_t(interface_t view);

        view_t(sardine::accessor<T> a, s_view i)
            : base_t(std::move(a))
            , impl(std::move(i))
        {}

        interface_t view() const {
            return this->convert(impl->view());
        }

        sardine::url url() const {
            auto url = impl->url();
            this->update_url(url);
            return url;
        }

        template<typename NT>
        auto clone_with_new_accessor(sardine::accessor<NT> new_accessor) const -> view_t<NT> {
            return view_t<NT>( new_accessor, impl );
        }

    };

    template <typename T, typename Ctx>
    struct producer : buffer::view_base< T, producer< T, Ctx > >
    {
        using base_t = buffer::view_base< T, producer >;
        using base_t::accessor_t;
        using base_t::accessor;

        using interface_t = buffer::interface_type<T>;

        s_producer<Ctx> impl;

        producer(interface_t view);

        producer(sardine::accessor<T> a, s_producer<Ctx> i)
            : base_t(std::move(a))
            , impl(std::move(i))
        {}

        interface_t view() const {
            return this->convert(impl->view());
        }

        void send(Ctx& ctx) { impl->send(ctx); }
        void revert(Ctx& ctx) { impl->revert(ctx); }

        // static auto load(const json::value& jv) -> result<producer>;

        sardine::url url() const {
            auto url = impl->url();
            this->update_url(url);
            return url;
        }

        template<typename NT>
        auto clone_with_new_accessor(sardine::accessor<NT> new_accessor) const -> producer<NT, Ctx> {
            return producer<NT, Ctx>( new_accessor, impl );
        }

        // sardine::url url() const { return impl->url(); }

        // static auto open(json_t json, map_t parameter = map_t()) -> result<producer>;
        static auto open(url_view u) -> result<producer>;
    };

    template <typename T, typename Ctx>
    struct consumer : buffer::view_base< T, consumer< T, Ctx > >
    {
        using interface_t = buffer::interface_type<T>;

        using base_t = buffer::view_base< T, consumer >;
        using base_t::accessor_t;
        using base_t::accessor;

        s_consumer<Ctx> impl;

        consumer(interface_t view);

        consumer(sardine::accessor<T> a, s_consumer<Ctx> i)
            : base_t(std::move(a))
            , impl(std::move(i))
        {}

        interface_t view() const {
            return this->convert(impl->view());
        }

        void recv(Ctx& ctx) { impl->recv(ctx); }
        void revert(Ctx& ctx) { impl->revert(ctx); }

        static auto load(const json::value& jv) -> result<consumer>;

        sardine::url url() const {
            auto url = impl->url();
            this->update_url(url);
            return url;
        }


        template<typename NT>
        auto clone_with_new_accessor(sardine::accessor<NT> new_accessor) const -> consumer<NT, Ctx> {
            return consumer<NT, Ctx>( new_accessor, impl );
        }
        // sardine::url url() const { return impl->url(); }

        // static auto open(json_t json, map_t parameter = map_t()) -> result<consumer>;
        static auto open(url_view u) -> result<consumer>;

    };

    template<typename T, typename Ctx>
    struct factory
    {

        s_factory<Ctx> impl;
        [[no_unique_address]] sardine::accessor<T> accessor;

        factory(s_factory<Ctx> impl, sardine::accessor<T> accessor)
            : impl(std::move(impl))
            , accessor(accessor)
        {}

        static auto create_impl(url_view u) -> result< s_factory<Ctx> >;
        static auto create(url_view u) -> result<factory>;

        result< producer<T, Ctx> > create_producer() {
            return impl->create_producer().map([&](s_producer<Ctx> s_prod) {
                return producer<T, Ctx>{ accessor, std::move(s_prod) };
            });
        }

        result< consumer<T, Ctx> > create_consumer() {
            return impl->create_consumer().map([&](s_consumer<Ctx> s_cons) {
                return consumer<T, Ctx>{ accessor, std::move(s_cons) };
            });
        }

    };

} // namespace sardine
