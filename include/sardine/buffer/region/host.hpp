#pragma once

#include <sardine/accessor.hpp>
#include <sardine/buffer/base.hpp>
#include <sardine/buffer/adaptor.hpp>

#include <sardine/region/host.hpp>

namespace sardine::region::host
{

    /**
     * @brief A producer/consumer that uses a shared memory region as storage.
     *
     * Since producer and consumer are the same, we use the same struct for both.
     *
     */
    template<typename Ctx>
    struct producer_consumer
    {
        span_b shm_data;
        sardine::url u;

        producer_consumer(span_b data, sardine::url u)
            : shm_data(data)
            , u(std::move(u))
        {}

        span_b view() {
            return shm_data;
        }

        void recv(Ctx&) {}
        void send(Ctx&) {}
        void revert(Ctx&) {}

        url_view url() const { return u; }
    };

    template<typename Ctx>
    struct factory : sardine::buffer::interface::factory<Ctx>
    {
        span_b shm_data;
        md_descriptor_impl md_descriptor;
        url u;

        factory(span_b shm_data, md_descriptor_impl md_descriptor, url_view u)
            : shm_data(shm_data)
            , md_descriptor(std::move(md_descriptor))
            , u(u)
        {}

        std::size_t offset() const override { return md_descriptor.offset(); }
        std::span<const std::size_t> extents() const override { return md_descriptor.extents(); }
        bool is_strided() const override { return md_descriptor.is_strided(); }
        std::span<const std::size_t> strides() const override { return md_descriptor.strides(); }

        result< s_producer<Ctx> > create_producer() override {
            return buffer::make_s_producer<Ctx>(producer_consumer<Ctx>{shm_data, u});
        }

        result< s_consumer<Ctx> > create_consumer() override {
            return buffer::make_s_consumer<Ctx>(producer_consumer<Ctx>{shm_data, u});
        }

        static result< s_factory<Ctx> > create(url_view u) {
            auto memory_h = bytes_from_url(u);
            EMU_TRUE_OR_RETURN_ERROR(memory_h);

            auto md = make_md_descriptor(u.params());
            EMU_TRUE_OR_RETURN_ERROR(md);

            return std::make_shared<factory>(*memory_h, *std::move(md), u);
        }
    };

} // namespace sardine::region::host
