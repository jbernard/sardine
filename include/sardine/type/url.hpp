#pragma once

#include <sardine/concepts.hpp>
#include <sardine/type.hpp>
#include <sardine/type/json.hpp>

#include <boost/url.hpp>
#include <boost/lexical_cast.hpp>

#include <fmt/format.h>
#include <fmt/ostream.h>

namespace sardine
{

    using boost::urls::url;
    using boost::urls::url_view;

namespace urls
{
    using boost::urls::params_view;
    using boost::urls::params_ref;

    using boost::urls::parse_uri;
    using boost::urls::parse_uri_reference;

    using boost::urls::ignore_case;

    template<typename T>
    optional<T> try_parse_at(params_view params, std::string_view key) {
        if (auto it = params.find(key); it != params.end()) {
            const auto& param = *it;
            if ( not param.has_value )
                return T{};
            else
                return json::opt_to<T>(json::parse(param.value));
        }
        return nullopt;
    }

    inline optional<std::string> try_get_at(params_view params, std::string_view key) {
        if (auto it = params.find(key); it != params.end()) {
            const auto& param = *it;
            if ( not param.has_value )
                return std::string{};
            else
                return param.value;
        }
        return nullopt;
    }

} // namespace urls

} // namespace sardine

namespace boost::urls
{

    void tag_invoke( json::value_from_tag, json::value& jv, url_view url );

    json::result< url > tag_invoke( json::try_value_to_tag< url >, const json::value& jv );

    void tag_invoke( json::value_from_tag, json::value& jv, params_view params );

    // We are only interested by converting to json.
    json::result< params_ref > tag_invoke( json::try_value_to_tag< params_ref >, const json::value& jv ) = delete;

} // namespace boost::urls

template <sardine::cpts::url Url> struct fmt::formatter<Url> : ostream_formatter {};

template<typename CharT>
struct fmt::formatter<boost::core::basic_string_view<CharT>> : fmt::formatter<fmt::basic_string_view<CharT>> {
    template <typename FormatContext>
    auto format(boost::core::basic_string_view<CharT> data, FormatContext &ctx) const {
        return fmt::formatter<fmt::basic_string_view<CharT>>::format({data.data(), data.size()}, ctx);
    }
};
