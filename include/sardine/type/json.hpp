#pragma once

#include <sardine/utility.hpp>

#include <emu/macro.hpp>
#include <emu/utility.hpp>

#include <boost/json.hpp>
#include <boost/callable_traits/args.hpp>
#include <boost/callable_traits/return_type.hpp>

namespace sardine::json
{

    //TODO: use specific types instead
    namespace bj = boost::json;
    using bj::value;
    using bj::object;
    using bj::array;
    using bj::string;

    using bj::serialize;
    using bj::parse;

    using bj::value_to;
    using bj::value_from;
    // using bj::try_value_to;

    template<typename Value>
    optional<object> try_object(Value&& value) {
        if (value.is_object())
            return EMU_FWD(value).as_object();
        else
            return nullopt;
    }

    template<typename Value>
    optional<array> try_array(Value&& value) {
        if (value.is_array())
            return EMU_FWD(value).as_array();
        else
            return nullopt;
    }

    template<typename Value>
    optional<string> try_string(Value&& value) {
        if (value.is_string())
            return EMU_FWD(value).as_string();
        else
            return nullopt;
    }

    template<typename Fn, std::convertible_to<std::string_view>... Names>
    constexpr auto try_find_all(Fn&& f, const object& obj, Names&&... names) {
        namespace ct = boost::callable_traits;

        return [&] (auto... its) -> result< ct::return_type_t<Fn> > {
            if (((its != obj.end()) && ...))
                return f(its->value()...);
            else
                return bj::make_error_code( bj::error::not_found );
        } (obj.find(names)...);
    }

    template<typename Fn, std::convertible_to<std::string_view>... Names>
    auto try_invoke_if_all(Fn&& f, const object& obj, Names&&... names) {
        namespace ct = boost::callable_traits;

        return try_find_all([&f] (const auto&... values) -> result< ct::return_type_t<Fn> > {
            return emu::invoke_with_args([&]<typename... Ts> (emu::type_pack<Ts...>) {
                return [&f] (auto&&... values) -> result< ct::return_type_t<Fn> > {
                    if((values.has_value() && ...))
                        return f(EMU_FWD(values).value()...);
                    else
                        return emu::get_first_error(as_result(EMU_FWD(values))...);
                } (bj::try_value_to<Ts>(values)...);
            }, /*reference= */ f);
        }, obj, EMU_FWD(names)...);
    }

    template<typename T>
    result<T> try_value_to(const value& j) {
        return as_result(bj::try_value_to<T>(j));
    }

    template<typename T>
    optional<T> opt_to(const value& j) {
        auto res = json::try_value_to<T>(j);
        if(res.has_value())
            return res.value();
        else
            return nullopt;
    }

    template<typename T>
    optional<T> opt_to(const object& obj, std::string_view key) {
        value* it = obj.if_contains(key);
        if(it)
            return opt_to<T>(*it);
        else
            return nullopt;
    }

    template<typename T>
    T value_or(const value& j, T&& def) {
        return opt_to< std::decay_t<T> >(j).value_or(EMU_FWD(def));
    }

    template<typename T>
    T value_or(const object& j, std::string_view key, T&& def) {
        return opt_to< std::decay_t<T> >(j, key).value_or(EMU_FWD(def));
    }

    /**
     * @brief Try to convert a value to an object, access a key and convert it to T.
     * If any of the steps fail, return def.
     *
     * @tparam T
     * @param j
     * @param key
     * @param def
     * @return T
     */
    template<typename T>
    T value_or(const value& j, std::string_view key, T&& def) {
        auto maybe_object = try_object(j).and_then([&] (const object& obj) {
            return opt_to< std::decay_t<T> >(obj, key);
        }).value_or(EMU_FWD(def));

    }

} // namespace sardine::json
