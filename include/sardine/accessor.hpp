#pragma once

#include <sardine/type.hpp>
#include <sardine/type/url.hpp>

#include <emu/span.hpp>
#include <emu/mdspan.hpp>

#include <algorithm>

namespace sardine
{

    constexpr auto extent_key = "extent";
    constexpr auto stride_key = "stride";
    constexpr auto offset_key = "offset";
    constexpr auto element_size_key = "element_size";

namespace interface
{

    struct md_descriptor
    {
        virtual ~md_descriptor() = default;

        virtual std::size_t offset() const = 0;
        virtual std::span<const std::size_t> extents() const = 0;
        virtual bool is_strided() const { return false; }
        virtual std::span<const std::size_t> strides() const { return {}; }
    };

} // namespace interface

    struct md_descriptor_impl : interface::md_descriptor
    {
        std::vector<std::size_t> extents_;
        std::vector<std::size_t> strides_;
        std::size_t offset_;

        md_descriptor_impl(std::vector<std::size_t> extents, std::vector<std::size_t> strides, std::size_t offset)
            : extents_(std::move(extents))
            , strides_(std::move(strides))
            , offset_(offset)
        {}

        std::size_t offset() const override { return offset_; }
        std::span<const std::size_t> extents() const override { return extents_; }
        bool is_strided() const override { return not strides_.empty(); }
        std::span<const std::size_t> strides() const override { return strides_; }
    };

    // inline result<md_descriptor_impl> make_md_descriptor(url_view u) {
    //     using vector_t = std::vector<std::size_t>;

    //     auto params = u.params();

    //     auto extents = json::opt_to<vector_t>(json.as_object(), extent_key).value_or(vector_t{1});

    //     auto strides = json.opt<vector_t>(stride_key).value_or(vector_t{});

    //     return md_descriptor_impl(std::move(extents), std::move(strides));
    // }

    inline result<md_descriptor_impl> make_md_descriptor(urls::params_view params) {
        using vector_t = std::vector<std::size_t>;

        auto extents = urls::try_parse_at<vector_t>(params, extent_key).value_or(vector_t{1});

        auto strides = urls::try_parse_at<vector_t>(params, stride_key).value_or(vector_t{});

        auto offset = urls::try_parse_at<std::size_t>(params, offset_key).value_or(0);

        return md_descriptor_impl(std::move(extents), std::move(strides), offset);
    }

    template<typename Mapping>
    auto create_mapping(const interface::md_descriptor& f) -> result< Mapping > {
        using Extent = typename Mapping::extents_type;
        using Layout = typename Mapping::layout_type;

        using array_t = std::array<std::size_t, Extent::rank()>;

        array_t extents; std::ranges::copy(f.extents(), extents.begin());

        constexpr static auto rank = Mapping::extents_type::rank();

        if (extents.size() != rank)
            return make_unexpected(error::accessor_rank_mismatch);

        if constexpr ( std::same_as<Layout, emu::layout_right>
                    or std::same_as<Layout, emu::layout_left> ) {
            return Mapping(extents);

        } else if constexpr (std::same_as<Layout, emu::layout_stride> ) {

            if (not f.is_strided())
                // not having stride if fine, we can compute it.
                return Mapping(extents);
            else {
                array_t strides; std::ranges::copy(f.strides(), strides.begin());

                if (strides.size() != rank)
                    return make_unexpected(error::accessor_rank_mismatch);

                return Mapping(extents, strides);
            }

        } else
            static_assert(emu::dependent_false<Mapping>, "Layout is not supported.");
    }

    template<typename T>
    struct accessor
    {
        using view_type = T;
        using value_type = T;

        std::size_t offset_;

        static result< accessor > create(const interface::md_descriptor& f) {
            if (f.extents().size() > 1)
                return make_unexpected(error::accessor_not_scalar);
            return accessor{f.offset()};
        }

        static accessor from_view(const T&) {
            return accessor(0);
        }

        accessor(std::size_t offset)
            : offset_(offset)
        {}

        accessor(const accessor& other) = default;

        std::size_t size() const { return 1; }
        std::size_t offset() const { return offset_; }

        T& convert(span_b buffer) const {
            EMU_ASSERT_MSG(buffer.size() >= sizeof(T), "Buffer is too small to hold the value.");
            return *(reinterpret_cast<T*>(buffer.data()) + offset_);
        }

        // span_b to_bytes(T& value) const {
        //     return std::as_writable_bytes(std::span{&value, 1});
        // }

        void copy(const T& src, T& dst) const {
            dst = src;
        }

        void update_url(url& u) const {
            auto params = u.params();

            params.set(offset_key, std::to_string(offset_));
            // params.set(extent_key, "[1]"); // not necessary, already default value
        }
    };

    template<typename T>
    struct accessor< std::span<T> >
    {
        using view_type = std::span<T>;
        using value_type = T;

        std::size_t offset_;
        std::size_t size_;

        static result< accessor > create(const interface::md_descriptor& f) {
            std::size_t size = 1;
            for (auto e : f.extents()) size *= e;

            if (f.is_strided()) {
                //TODO: check if strides are c contiguous
            }
            return accessor{f.offset(), size};
        }

        static accessor from_view(const std::span<T>& value) {
            return accessor(0, value.size());
        }

        accessor(std::size_t offset, std::size_t size)
            : offset_(offset)
            , size_(size)
        {}

        accessor(const accessor& other) = default;

        std::size_t size() const { return size; }
        std::size_t offset() const { return offset_; }
        std::size_t lead_stride() const { return 1; }

        std::span<T> convert(span_b buffer) const {
            return emu::as_t<T>(buffer).subspan(offset_, size_);
        }

        accessor<T> close_lead_dim() const {
            return {offset_};
        }

        void copy(std::span<const T> src, std::span<T> dst) const {
            std::ranges::copy(src, dst.begin()); // ! doesn't work with cuda span
        }

        void update_url(url& u) const {
            auto params = u.params();

            params.set(offset_key, std::to_string(offset_));
            params.set(extent_key, fmt::format("[{}]", size_));
        }

    protected:
        //! WARNING, offset and size input are probably in element but members are in bytes !
        // possible to fix that
        accessor subspan(std::size_t new_offset, std::size_t new_size = std::dynamic_extent) const
        {
            // Paranoia check, it is required that std::byte is 1 byte long
            // for the offset computation to work.
            static_assert(sizeof(std::byte) == 1, "std::byte is not 1 byte long.");

            // Create a fake span from offset and size.
            std::span fake_span{reinterpret_cast< std::byte* >(offset_), size_};

            // let span do the computation to get the new subspan.
            auto sv = fake_span.subspan(new_offset, new_size);

            // sv.data() is the new offset and sv.size() is the new size.
            return {reinterpret_cast<std::size_t>(sv.data()), sv.size()};
        }
    };

    template<emu::cpts::mdspan MdSpan>
    struct accessor< MdSpan >
    {
        using view_type = MdSpan;
        using value_type = typename MdSpan::value_type;
        using element_type = typename MdSpan::element_type;
        using mapping_type = typename MdSpan::mapping_type;

        std::size_t offset_;
        mapping_type mapping_;

        static result< accessor > create(const interface::md_descriptor& f) {
            return create_mapping<mapping_type>(f).map([&f](auto mapping) {
                return accessor(f.offset(), mapping);
            });
        }

        static accessor from_view(const MdSpan& mdspan) {
            return accessor(0, mdspan.mapping());
        }

        accessor(std::size_t offset, mapping_type mapping)
            : offset_(offset)
            , mapping_(std::move(mapping))
        {}

        accessor(const accessor& other) = default;

        std::size_t size() const { return mapping_.required_span_size(); }
        std::size_t offset() const { return offset_; }
        std::size_t lead_stride() const { return mapping_.stride(0); } // what about layout_f ?

        auto close_lead_dim() const {
            return submdspan_accessor(0);
        }

        MdSpan convert(span_b buffer) const {
            return MdSpan(reinterpret_cast<element_type*>(buffer.data()) + offset_, mapping_);
        }

        // span_b to_bytes(MdSpan value) const {
        //     return std::as_writable_bytes(value);
        // }

        void copy(const MdSpan& src, MdSpan& dst) const {
            //TODO: implement copy that works for mdspan (with strides support)
            std::copy_n(src.data_handle(), mapping_.required_span_size(), dst.data_handle());
        }

        // void update_json(json::object& jo) const {

        //     jo[extent_key] = json::value_from(mapping.extents()); // or use fmt::format("[{}]", emu::extent(value, ","))

        //     if constexpr (not value.is_always_exhaustive())
        //         if (not value.is_exhaustive())
        //             jo[stride_key] = json::value_from(mapping.strides()); // or use fmt::format("[{}]", emu::stride(value, ","))
        // }

        void update_url(url& u) const {
            auto params = u.params();

            params.set(extent_key, fmt::format("[{}]", emu::extent(mapping_, ",")));
            params.set(offset_key, std::to_string(offset_));
            if constexpr (not mapping_.is_always_exhaustive())
                if (not mapping_.is_exhaustive())
                    params.set(stride_key, fmt::format("[{}]", emu::stride(mapping_, ",")));
        }

    protected:
        template<class... SliceSpecifiers>
        auto submdspan_accessor(SliceSpecifiers... specs) const
        {
            // Paranoia check, it is required that std::byte is 1 byte long
            static_assert(sizeof(std::byte) == 1, "std::byte is not 1 byte long.");

            // Create a fake mdspan from offset and size. Use deduction guide.
            emu::mdspan fake_mdspan(reinterpret_cast< std::byte* >(offset_), this->mapping_);

            // let mdspan do the computation to get the new submdspan.
            auto sv = emu::submdspan(fake_mdspan, specs...);

            using new_mapping_t = typename decltype(sv)::mapping_type;
            using new_mdspan_t = emu::mdspan<element_type, typename new_mapping_t::extents_type, typename new_mapping_t::layout_type>;

            return sardine::accessor<new_mdspan_t>{reinterpret_cast<std::size_t>(sv.data_handle()), sv.mapping()};
        }
    };

    //TODO: implement submdspan for accessor

    // template<typename T>
    // auto make_accessor(const interface::md_descriptor& f) -> accessor<T> {
    //     return accessor<T>::create(f);
    // }

    // template<typename T>
    // auto make_accessor(json_t json) -> result< accessor<T> > {
    //     return make_md_descriptor(json).map([](auto md){
    //         return make_accessor<T>(md);
    //     });
    // }

    template<typename T>
    auto make_accessor(urls::params_view param) -> result< accessor<T> > {
        return make_md_descriptor(param).map([](auto md){
            return accessor<T>::create(md);
        });
    }

    template<typename T>
    auto from_view(T && view) -> accessor< emu::decay<T> > {
        return accessor<emu::decay<T>>::from_view(EMU_FWD(view));
    }

    // template<typename T>
    // void update_json(json::value& jv, const T& value) {
    //     accessor<T>(value).update_json(jv);
    // }

    template<typename T>
    void update_url(url& u, const T& value) {
        from_view(value).update_url(u);
    }

    // Adds python accessor
    // template<>
    // struct accessor< nb::ndarray >
    // {
    //     using array_t = nb::ndarray;

    //     emu::container<size_t> shape;
    //     nb::handle owner = nanobind::handle();
    //     emu::container<int64_t> strides;
    //     nb::dlpack::dtype dtype;

    //     accessor(const factory& f, nb::dlpack::dtype dt)
    //         : dtype(dt)
    //     {
    //         auto extents = f.extent();
    //         shape = std::view_converter(extents);
    //         if (f.is_strided())
    //             strides = std::vector<int64_t>(f.strides());
    //     }

    //     nb::ndarray convert(span_b buffer) const {
    //         return nb::ndarray(
    //             emu::nanobind::void_ptr(buffer.data()),
    //             shape.size(),
    //             shape.data(),
    //             owner,
    //             strides.data(), // maybe null
    //             dtype
    //         );

    //     void copy(const nb::ndarray& src, nb::ndarray& dst) const {
    //         nb::dict d;
    //         d["src"] = src;
    //         d["dst"] = dst;

    //         // delegate the copy to numpy
    //         nb::exec("dst[()] = src", /*global = */ std::move(d));

    //     }

    // };


} // namespace sardine
