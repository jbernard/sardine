#pragma once

// #include <cuda/runtime_api.hpp>

#include <memory>

namespace sardine
{

    struct host_ctx {

    };

    // cuda_ctx derive from host_ctx. That mean that we can provide a cuda_ctx to a host function
    // it will work as long as we don't try to access data that was updated along the cuda_ctx
    // In this case, we need to synchronize the stream before calling the host_ctx function.
    struct cuda_ctx : host_ctx {
        // std::shared_ptr<cuda::stream_t> stream_;

        // cuda_ctx(cuda::device_t device)
        //     : stream_{std::make_shared<cuda::stream_t>(cuda::stream::create(device, false))}
        // {}

        // cuda_ctx(cuda::stream_t stream)
        //     : stream_{std::make_shared<cuda::stream_t>(std::move(stream))}
        // {}

        // cuda::stream_t& stream() const {
        //     return *stream_;
        // }

        // cuda::device_t device() const {
        //     return stream_->device();
        // }
    };

} // namespace sardine
