#pragma once

#include <sardine/type.hpp>

#include <fmt/format.h>

#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/hana/fwd/core/make.hpp>

#include <type_traits>
#include <stdexcept>
#include <ranges>
#include <string_view>

namespace sardine
{

    template<typename T>
    constexpr result<T> as_result(boost::system::result<T> res) {
        if (res)
            return res.value();
        else
            return unexpected(res.error());
    }

    inline void remove(cstring_view name) {
        boost::interprocess::shared_memory_object::remove(name.c_str());
    }

    // auto split_string(std::string_view words, std::string_view delim = " ") {
    //     return std::views::split(words, delim) | std::views::transform([](auto sr) {
    //         return std::string_view(&*sr.begin(), std::ranges::distance(sr));
    //     });
    // }

    // Handle reference, span and mdspan
    // converts a value to a span of bytes, automatically detect constness
    template<typename View>
    constexpr auto as_span_of_bytes(View&& value) {
        using d_view = emu::decay<View>;


        if constexpr (emu::cpts::span< d_view >) {
            if constexpr (std::is_const_v<typename d_view::element_type>)
                return std::as_bytes(value);
            else
                return std::as_writable_bytes(value);

        } else if constexpr (emu::cpts::mdspan< d_view >) {
            if constexpr (std::is_const_v<typename d_view::element_type>)
                return std::as_bytes(std::span{value.data_handle(), value.mapping().required_span_size()});
            else
                return std::as_writable_bytes(std::span{value.data_handle(), value.mapping().required_span_size()});

        } else
            if constexpr (std::is_const_v<View>)
                return std::as_bytes(std::span{&value, 1});
            else
                return std::as_writable_bytes(std::span{&value, 1});
    }


namespace detail
{

    template<typename T, typename Tuple>
    struct is_constructible_from_tuple_args : std::false_type {};

    template<typename T, typename ...Args>
    struct is_constructible_from_tuple_args<T, std::tuple<Args...>> : std::is_constructible<T, Args...> {};

} // namespace detail

    template<typename T, typename ...Args>
    constexpr bool is_constructible_from_tuple_args_v = detail::is_constructible_from_tuple_args<T, Args...>::value;

    template<typename Map, typename Key, typename Fn>
    auto auto_emplace(Map & map, const Key & key, Fn && fn) {

        using result_type = std::invoke_result_t<Fn>;

        if constexpr (std::is_constructible_v<typename Map::mapped_type, result_type>)
            return map.emplace(key, fn());
        else
        {
            static_assert(
                is_constructible_from_tuple_args_v<typename Map::mapped_type, result_type>,
                "Mapped type is not constructible from the result of the function"
            );

            return map.emplace(
                std::piecewise_construct,
                std::forward_as_tuple(key),
                fn());
        }
    }

    template<typename Map, typename Key, typename Fn>
    auto find_or_emplace(Map & map, const Key & key, Fn && fn)
    {
        auto it = map.find(key);
        if (it == map.end())
            it = auto_emplace(map, key, fn).first;
        return it;
    }

    template<typename Map, typename Key, typename Fn>
    auto emplace_or_throw(Map & map, const Key & key, Fn && fn)
    {
        auto it = map.find(key);
        if (it != map.end())
            throw std::runtime_error("Key already exists");
        return auto_emplace(map, key, fn).first;
    }

    using boost::hana::make;

} // namespace sardine
