#pragma once

namespace std
{

    struct error_code;

} // namespace std


namespace boost::urls
{

    struct url;
    struct url_view;

    struct params_view;
    struct params_ref;

    struct parse_uri;
    struct parse_uri_reference;

} // namespace boost::urls

namespace boost::json
{

    struct value;


} // namespace boost::json


namespace tl
{

    template<typename T, typename E>
    struct expected;

} // namespace tl


namespace sardine
{

    template<typename T>
    using result = tl::expected<T, std::error_code>;

    using boost::urls::url;
    using boost::urls::url_view;

namespace urls
{
    using boost::urls::params_view;
    using boost::urls::params_ref;

    using boost::urls::parse_uri;
    using boost::urls::parse_uri_reference;

} // namespace urls

} // namespace sardine
