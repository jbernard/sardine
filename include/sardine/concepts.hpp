#pragma once

#include <sardine/fwd.hpp>

#include <emu/concepts.hpp>

namespace sardine::cpts
{

    template<typename T>
    concept url = std::same_as<T, sardine::url>
               or std::same_as<T, sardine::url_view>;

    template<typename T>
    concept url_params = std::same_as<T, sardine::urls::params_ref>
                      or std::same_as<T, sardine::urls::params_view>;

    template<typename T>
    concept url_aware = requires(const T& t) {
        { t.url() } -> url;
    };

    template<typename T>
    concept loadable = requires(const boost::json::value& jv) {
        { T::load(jv) } -> std::same_as< result<T> >;
    };

    template<typename T>
    concept view = emu::cpts::span<T>
                or emu::cuda::cpts::span<T>;

    template<typename T>
    concept mdview = emu::cpts::mdspan<T>
                  or emu::cuda::cpts::mdspan<T>;

    template<typename T>
    concept all_view = view<T> or mdview<T>;

    template<typename T>
    concept closable_lead_dim = requires(const T& t) {
        { t.close_lead_dim() };
    };

} // namespace sardine::cpts
