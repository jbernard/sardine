#pragma once

#include <sardine/type/json.hpp>
#include <sardine/type/url.hpp>

namespace sardine
{

    template <typename T>
    result<T> load( const json::value& jv ) {
        if constexpr (cpts::loadable<T>)
            return T::load(jv);

        // Try to convert the json into a url or check if it contains a one.
        result<url> maybe_url;

        if constexpr (not std::is_same_v<T, std::string>)
            // Special case when requesting a std::string, we don't want to accidentally parse it to a url.
            // url support is done by explicitly putting it inside jv["_url"]
            maybe_url = json::try_value_to< url >( jv );

        // If there is not url, we check if the json has a url inside "_url" key.
        if (not maybe_url and jv.is_object() and jv.contains("_url")) {
            maybe_url = json::try_value_to< url >( jv["_url"] );
        }

        EMU_RETURN_IF_VALUE(maybe_url.map( &from_url<T> ));

        return json::try_value_to<T>( jv );

    }

} // namespace sardine
