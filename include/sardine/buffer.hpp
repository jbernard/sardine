#pragma once

#include <sardine/buffer/base.hpp>
#include <sardine/buffer/adaptor.hpp>
#include <sardine/buffer/ring.hpp>
#include <sardine/buffer/region/host.hpp>
// #include <sardine/buffer/managed.hpp>

namespace sardine
{

    template<typename Ctx, typename T>
    producer<std::decay_t<T>, Ctx> make_producer(T&& value) {
        return producer<std::decay_t<T>, Ctx>(EMU_FWD(value));
    }

    template<typename Ctx, typename T>
    consumer<std::decay_t<T>, Ctx> make_consumer(T&& value) {
        return consumer<std::decay_t<T>, Ctx>(EMU_FWD(value));
    }

    template <typename T, typename Ctx>
    box<T, Ctx>::box(interface_t view)
        : base_t(from_view(view))
        , prod_impl(buffer::make_s_producer<Ctx>(buffer::native::producer<host_ctx>{as_span_of_bytes(view)}))
        , cons_impl(buffer::make_s_consumer<Ctx>(buffer::native::consumer<host_ctx>{as_span_of_bytes(view)}))
        , storage(cons_impl->view())
        , value(storage.init(accessor().convert(cons_impl->view())))
    {}

    template <typename T>
    view_t<T>::view_t(interface_t view)
        : base_t(from_view(view))
        , impl(buffer::make_s_view(buffer::native::view_t{as_span_of_bytes(view)}))
    {}

    template <typename T, typename Ctx>
    producer<T, Ctx>::producer(interface_t view)
        : base_t(from_view(view))
        , impl(buffer::make_s_producer<Ctx>(buffer::native::producer<Ctx>{as_span_of_bytes(view)}))
    {}

    template <typename T, typename Ctx>
    consumer<T, Ctx>::consumer(interface_t view)
        : base_t(from_view(view))
        , impl(buffer::make_s_consumer<Ctx>(buffer::native::consumer<Ctx>{as_span_of_bytes(view)}))
    {}

    template<typename T, typename Ctx>
    auto factory<T, Ctx>::create_impl(url_view u) -> result<s_factory<Ctx>> {
        auto scheme = u.scheme();

        // using proxy_t = buffer::proxy_type<T>;

        if (scheme == ring::url_scheme)
            return ring::factory<Ctx>::create(u);
        // if (scheme == json::url_scheme)
        //     return json::factory<proxy_t, Ctx>::create(u);
        else if (scheme == region::host::url_scheme)
            return region::host::factory<Ctx>::create(u);
        // else if (scheme == managed::url_scheme)
        //     return managed::factory<Ctx>::create(u);
        else
            return make_unexpected(error::url_unknown_scheme);

    }

    template<typename T, typename Ctx>
    auto factory<T, Ctx>::create(url_view u) -> result<factory> {
        auto scheme = u.scheme();

        result<s_factory<Ctx>> res = create_impl(u);
        EMU_TRUE_OR_RETURN_ERROR( res );

        return sardine::accessor<T>::create(*res.value())
            .map([&](auto accessor) {
                return factory{res.value(), accessor};
            });
    }

    template<typename T, typename Ctx>
    auto box<T, Ctx>::open(url_view u) -> result<box> {
        return factory<T, Ctx>::create(u)
            .and_then([](auto f) -> result<box> {
                auto prod = f.impl->create_producer();
                EMU_TRUE_OR_RETURN_ERROR( prod );
                auto cons = f.impl->create_consumer();
                EMU_TRUE_OR_RETURN_ERROR( cons );
                return box<T, Ctx>(f.accessor, *prod, *cons);
            }
        );
    }

    template<typename T, typename Ctx>
    auto producer<T, Ctx>::open(url_view u) -> result<producer> {
        return factory<T, Ctx>::create(u)
            .and_then([](auto f) {
                return f.create_producer();
            }
        );
    }

    template<typename T, typename Ctx>
    auto consumer<T, Ctx>::open(url_view u) -> result<consumer> {
        return factory<T, Ctx>::create(u)
            .and_then([](auto f) {
                return f.create_consumer();
            }
        );
    }

    // template<typename T, typename Ctx>
    // auto factory<T, Ctx>::create(const json::value& jv) -> result<factory> {
    //     return json::factory<T, Ctx>::create(jv)
    //         .and_then([](auto&& p_impl) {
    //             return sardine::accessor<T>::create(*p_impl).map([&](auto accessor) {
    //                 return factory{p_impl, accessor};
    //             });
    //         });
    // }

    // // template<typename T>
    // // auto box<T>::load(const json::value& jv) -> result<box> {
    // //     return factory<T, host_ctx>::create(jv)
    // //         .and_then([](auto f) {
    // //             return f.create_box();
    // //         }
    // //     );
    // // }

    // template<typename T, typename Ctx>
    // auto consumer<T, Ctx>::load(const json::value& jv) -> result<consumer> {
    //     return factory<T, Ctx>::create(u)
    //         .and_then([](auto&& f) {
    //             return f.create_consumer();
    //         }
    //     );
    // }

    // template<typename T, typename Ctx>
    // auto producer<T, Ctx>::load(const json::value& jv) -> result<producer> {
    //     return factory<T, Ctx>::create(u)
    //         .and_then([](auto&& f) {
    //             return f.create_producer();
    //         }
    //     );
    // }

} // namespace sardine

template<typename T, typename Ctx, typename CharT>
struct fmt::formatter<sardine::box<T, Ctx>, CharT> {

    constexpr auto parse(format_parse_context& ctx) {
        return ctx.begin();
    }

    template<typename FormatContext>
    auto format(const sardine::box<T, Ctx>& box, FormatContext& ctx) {
        return fmt::format_to(ctx.out(), "value({})", box.value);
    }
};

template<typename T, typename Ctx, typename CharT>
struct fmt::formatter<sardine::producer<T, Ctx>, CharT> {

    constexpr auto parse(format_parse_context& ctx) {
        return ctx.begin();
    }

    template<typename FormatContext>
    auto format(const sardine::producer<T, Ctx>& value, FormatContext& ctx) {
        return fmt::format_to(ctx.out(), "producer({})", value.view());
    }
};

template<typename T, typename Ctx, typename CharT>
struct fmt::formatter<sardine::consumer<T, Ctx>, CharT> {

    constexpr auto parse(format_parse_context& ctx) {
        return ctx.begin();
    }

    template<typename FormatContext>
    auto format(const sardine::consumer<T, Ctx>& value, FormatContext& ctx) {
        return fmt::format_to(ctx.out(), "consumer({})", value.view());
    }
};
