#pragma once

#include <sardine/error.hpp>

#include <emu/type_traits.hpp>
#include <emu/expected.hpp>
#include <emu/optional.hpp>
#include <emu/cstring_view.hpp>
#include <emu/container.hpp>

#include <boost/system.hpp>
#include <boost/interprocess/creation_tags.hpp>

#include <cstddef>
#include <span>

namespace sardine
{

    using emu::cstring_view;

    using span_b = std::span<std::byte>;
    using span_cb = std::span<const std::byte>;

    using container_b = emu::container<std::byte>;
    using container_cb = emu::container<const std::byte>;

    using emu::optional;

    using emu::nullopt;

    template<typename T>
    using result = tl::expected<T, std::error_code>;

    using tl::unexpected;

    /**
     * @brief This is a helper type that is used to bypass the optional/expected limitation of not being able to hold a reference.
     *
     * @tparam T
     */
    template<typename T>
    using return_type = std::conditional_t<emu::is_ref<T>, std::reference_wrapper<std::remove_reference_t<T>>, T>;

    using boost::interprocess::create_only_t;
    using boost::interprocess::open_only_t;
    using boost::interprocess::open_read_only_t;
    using boost::interprocess::open_or_create_t;
    using boost::interprocess::open_copy_on_write_t;

    using boost::interprocess::create_only;
    using boost::interprocess::open_only;
    using boost::interprocess::open_read_only;
    using boost::interprocess::open_or_create;
    using boost::interprocess::open_copy_on_write;

    struct heap_allocator_tag {};

    constexpr heap_allocator_tag heap_allocator;

} // namespace sardine
