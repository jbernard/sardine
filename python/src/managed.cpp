#include <sardine/managed.hpp>
#include <sardine/python/managed_helper.hpp>

#include <emu/cstring_view.hpp>
#include <emu/cast/cstring_view.hpp>
#include <emu/cast/span.hpp>

#include <nanobind/nanobind.h>
#include <nanobind/make_iterator.h>
#include <nanobind/stl/string.h>

#include <boost/preprocessor/stringize.hpp>
#include <boost/callable_traits.hpp>

#include <functional>
#include <type_traits>

namespace nb = nanobind;
namespace sa = sardine;

nb::object open_obj(sa::managed_t& managed, emu::cstring_view name, nb::object type) {
    if (nb::hasattr(type, "__shm_open__"))
        return type.attr("__shm_open__")(managed, name);

    throw std::runtime_error("Unknown type");
}

nb::object create_obj(sa::managed_t& managed, emu::cstring_view name, nb::object type, nb::args args) {
    if (nb::hasattr(type, "__shm_create__"))
        return type.attr("__shm_create__")(managed, name, *args);

    throw std::runtime_error("Unknown type");
}

nb::object open_or_create_obj(sa::managed_t& managed, emu::cstring_view name, nb::object& type, nb::args args) {
    if (nb::hasattr(type, "__shm_open_or_create__"))
        return type.attr("__shm_open_or_create__")(managed, name, *args);

    throw std::runtime_error("Unknown type");
}

nb::object force_create_obj(sa::managed_t& managed, emu::cstring_view name, nb::object& type, nb::args args) {
    if (nb::hasattr(type, "__shm_force_create__"))
        return type.attr("__shm_force_create__")(managed, name, *args);

    throw std::runtime_error("Unknown type");
}

nb::object exist_obj(sa::managed_t& managed, emu::cstring_view name, nb::object& type) {
    if (nb::hasattr(type, "__shm_exist__"))
        return type.attr("__shm_exist__")(managed, name);

    throw std::runtime_error("Unknown type");
}

nb::object destroy_obj(sa::managed_t& managed, emu::cstring_view name, nb::object& type) {
    if (nb::hasattr(type, "__shm_destroy__"))
        return type.attr("__shm_destroy__")(managed, name);

    throw std::runtime_error("Unknown type");
}

namespace detail
{

    /**
     * @brief Take a method and change the first argument `this` to another type.
     *
     * The new function will have exactly the same signature (it won't be polymorphic)
     * and will be internally cast from the new type to the old type using operator *.
     *
     *
     * @tparam Fn
     * @tparam the new `this` type.
     */
    template <typename T, auto Met>
    auto adapt_method() {
        // Gets Method domain.
        using args = boost::callable_traits::args_t<decltype(Met)>;

        return []<typename This, typename... Args>(std::type_identity<std::tuple<This, Args...>>) {
            // This lambda which is returned have the exact same domain than `Met` except that
            // the first argument is `T` instead of `This`.
            return [] (T& t, Args... args) {
                // Call the original method with the provided args. We suppose '*t' returns `this`.
                return std::invoke(Met, *t, EMU_FWD(args)...);
            };
        }(std::type_identity<args>{});
    }

} // namespace detail


void register_shm_managed(nb::module_ m)
{
    using namespace nb::literals;

    nb::class_<sa::managed::named_value_t>(m, "named_value")
        .def_prop_ro("name", &sa::managed::named_value_t::name);

    nb::class_<sa::managed::named_range>(m, "named_range")
        .def("__len__", &sa::managed::named_range::size)
        .def("__iter__", [](sa::managed::named_range& range) {
                return nb::make_iterator(nb::type<sa::managed::named_range>(), "Iterator",
                                    range.begin(), range.end());
            }, nb::keep_alive<0, 1>())
    ;

    nb::class_<sa::managed_t> managed(m, "managed");

    managed
        .def("named", [](sa::managed_t& managed) {
            return managed.named();
        }, nb::keep_alive<0, 1>())
        .def("__repr__", [](sa::managed_t& managed) {
            auto total = managed.shm().get_size();
            auto used = total - managed.shm().get_free_memory();
            return fmt::format("sardine.shm.managed: used {}/{}", used, total);
        })
        .def("memory_available", [](sa::managed_t& managed) { return managed.shm().get_free_memory();})
        .def("memory_total",     [](sa::managed_t& managed) { return managed.shm().get_size(); })
        .def("memory_used",      [](sa::managed_t& managed) {
            return managed.shm().get_size() - managed.shm().get_free_memory();
        })
    ;

    using types = std::tuple<
        bool,
        int8_t,
        int16_t,
        int32_t,
        int64_t,
        uint8_t,
        uint16_t,
        uint32_t,
        uint64_t,
        // emu::half,
        float,
        double
    >;

    emu::product<types>([&]<typename T> {
        auto name = fmt::format("proxy_{}", emu::numeric_name<T>);

        using span_t = std::span<T>;

        struct proxy_scalar {
            sa::managed_t& managed;

            constexpr sa::managed_t& operator*() { return managed; }
        };

        nb::class_<proxy_scalar>(m, name.c_str())
            .def("open",
                 detail::adapt_method<proxy_scalar, &sa::managed_t::open<span_t>>(),
                 "name"_a)
            .def("create",
                 detail::adapt_method<proxy_scalar, &sa::managed_t::create<span_t, std::size_t, T>>(),
                 "name"_a, "size"_a = std::size_t(1), "value"_a = T{})
            .def("open_or_create",
                 detail::adapt_method<proxy_scalar, &sa::managed_t::open_or_create<span_t, std::size_t, T> >(),
                 "name"_a, "size"_a = std::size_t(1), "value"_a = T{})
            .def("force_create",
                 detail::adapt_method<proxy_scalar, &sa::managed_t::force_create<span_t, std::size_t, T>>(),
                 "name"_a, "size"_a = std::size_t(1), "value"_a = T{})
            // .def("set",
                    // detail::adapt_method<proxy_scalar, &sa::managed_t::set<T>>(),
                    // "name"_a, "value"_a = T{})
            .def("exist",
                 detail::adapt_method<proxy_scalar, &sa::managed_t::exist<span_t>>(),
                 "name"_a)
            .def("destroy",
                 detail::adapt_method<proxy_scalar, &sa::managed_t::destroy<span_t>>(),
                 "name"_a)
        ;

        managed.def_prop_ro(name.c_str(), [](sa::managed_t& managed){
            return proxy_scalar{managed};
        });

    });

    // {
    //     struct proxy_bytes {
    //         sa::managed_t& managed;

    //         constexpr sa::managed_t& operator*() { return managed; }
    //     };

    //     nb::class_<proxy_bytes>(m, "proxy_bytes")
    //         .def("open"          , detail::adapt_method<proxy_bytes, &sa::managed_t::open          <std::span<std::byte>             > >() , "name"_a          )
    //         .def("create"        , detail::adapt_method<proxy_bytes, &sa::managed_t::create        <std::span<std::byte>, std::size_t> >() , "name"_a, "size"_a)
    //         .def("open_or_create", detail::adapt_method<proxy_bytes, &sa::managed_t::open_or_create<std::span<std::byte>, std::size_t> >() , "name"_a, "size"_a)
    //         .def("force_create"  , detail::adapt_method<proxy_bytes, &sa::managed_t::force_create  <std::span<std::byte>, std::size_t> >() , "name"_a, "size"_a)
    //         .def("exist"         , detail::adapt_method<proxy_bytes, &sa::managed_t::exist         <std::span<std::byte>             > >() , "name"_a          )
    //         .def("destroy"       , detail::adapt_method<proxy_bytes, &sa::managed_t::destroy       <std::span<std::byte>             > >() , "name"_a          )
    //     ;

    //     managed.def_prop_ro("proxy_bytes", [](sa::managed_t& managed){
    //         return proxy_bytes{managed};
    //     });
    // }

    m.def("open",           &sa::managed::open,           "name"_a                       );
    m.def("create",         &sa::managed::create,         "name"_a, "initial_count"_a = 1);
    m.def("open_or_create", &sa::managed::open_or_create, "name"_a, "initial_count"_a = 1);

}




// nb::object open(sa::managed_t& managed, emu::cstring_view name, nb::object& type) {
//     if (nb::hasattr(type, "__shm_open__"))
//         return type.attr("__shm_open__")(managed, name);


//     // nb::module_ builtins = nb::module_::import("builtins");

//     // if      (type.is(builtins.attr("int")))
//     //     return managed.find<int>(name);
//     // else if (nb::isinstance<nb::float_>(obj))
//     //     return managed.find<>(name);
//     // else if (nb::isinstance<nb::bool_>(obj))
//     //     return managed.find<>(name);
//     // else if (nb::isinstance<nb::dict>(obj))
//     //     return managed.find<>(name);
//     // else if (nb::isinstance<nb::list>(obj))
//     //     return managed.find<>(name);
//     // else if (nb::isinstance<nb::str>(obj))
//     //     return managed.find<std::string>(name);
//     // else
//         throw std::runtime_error("Unknown type");
// }

