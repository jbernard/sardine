#include <sardine/semaphore.hpp>
#include <sardine/python/managed_helper.hpp>
#include <sardine/python/url_helper.hpp>

#include <nanobind/nanobind.h>

namespace nb = nanobind;

namespace sa = sardine;

void register_shm_semaphore(nb::module m)
{
    nb::class_<sa::semaphore_t> semaphore(m, "Semaphore");

    semaphore
        .def("release", [](sa::semaphore_t& sem, std::size_t count){
            for (std::size_t i = 0; i < count; ++i)
                sem.post();
        }, nb::arg("count") = 1)
        .def("acquire", [](sa::semaphore_t& sem, bool blocking) -> bool {
            if (blocking) {
                sem.wait();
                return true;
            }
            else
                return sem.try_wait();
        }, nb::arg("blocking") = true);

    auto initializer = [](sa::create_proxy proxy, std::size_t count) -> decltype(auto) {
        return proxy.create<sa::semaphore_t>(count);
    };

    sa::register_managed(semaphore, nb::return_value_policy::reference, initializer);
    sa::register_url(semaphore);
}
