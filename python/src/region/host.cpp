#include <sardine/shm/region/host.hpp>
#include <sardine/shm/region/host/manager.hpp>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

namespace sh = sardine::shm;

void register_shm_region_host(py::module m)
{
    py::class_<sh::region::host_handle>(m, "host_handle", py::buffer_protocol())
        .def("memory", [](sh::region::host_handle& h) -> py::memoryview {
            return py::memoryview::from_memory(
                h.get_address(),
                h.get_size(),
                /* readonly = */ false
            );
        })
        .def("size", &sh::region::host_handle::get_size)
        .def_buffer([](sh::region::host_handle& h) -> py::buffer_info { // not sure if relevant...
            return py::buffer_info(
                h.get_address(),
                sizeof(std::byte),
                py::format_descriptor<std::byte>::format(),
                1,
                { h.get_size() },
                { sizeof(std::byte) }
            );
        });

    m.def("open", [](const std::string& name) -> sh::region::host_handle& {
        return sh::region::host::manager::instance().open(name);
    }, py::return_value_policy::reference, py::arg("name"));

    m.def("create", [](const std::string& name, std::size_t size) -> sh::region::host_handle& {
        return sh::region::host::manager::instance().create(name, size);
    }, py::return_value_policy::reference, py::arg("name"), py::arg("size"));

    m.def("open_or_create", [](const std::string& name, std::size_t size) -> sh::region::host_handle& {
        return sh::region::host::manager::instance().open_or_create(name, size);
    }, py::return_value_policy::reference, py::arg("name"), py::arg("size"));


}