#include <nanobind/nanobind.h>

#include <sardine/python/cast/url.hpp>
// #include <sardine/utility.hpp>
// #include <sardine/value.hpp>

#include <emu/nanobind.hpp>

#include <cstddef>
#include <fmt/core.h>

namespace nb = nanobind;

void register_shm_managed(nb::module_ m);
void register_shm_json(nb::module_ m);
void register_shm_semaphore(nb::module_ m);
void register_shm_region_host(nb::module_ m);
void register_shm_region_device(nb::module_ m);

NB_MODULE(_sardine, m) {
    m.doc() = "nanobind _sardine plugin";

    m.def("fun", [](){
        fmt::print("fun!!\n");
    }, "A function!!");

    // m.def("url_of", [](nb::object obj) -> nb::object {
    //     if (nb::hasattr(obj, "__url_of__"))
    //         return obj.attr("__url_of__")();
    //     else
    //         throw std::runtime_error(fmt::format("{} does not have __url_of__", obj.get_type()));
    // });
    // m.def("from_url", [](nb::object url, nb::object type) -> nb::object {
    //     if (pybind11::hasattr(type, "__from_url__"))
    //         return type.attr("__from_url__")(url);
    //     else
    //         throw std::runtime_error(fmt::format("{} does not have __from_url__", type));
    // });
    m.def("from_url", [](sardine::url url, nb::object type) {
        fmt::print("from_url {}\n", url);
    });

    register_shm_managed(m.def_submodule("managed", "managed submodule"));
    // register_shm_json(m.def_submodule("json", "json submodule"));
    // register_shm_semaphore(m.def_submodule("semaphore", "semaphore submodule"));

    // auto region = m.def_submodule("region", "region submodule");
    // register_shm_region_host(region.def_submodule("host", "host region submodule"));
    // register_shm_region_device(region.def_submodule("device", "device region submodule"));
}