default: install

# Install sardine in conan cache and in pip
install *args:
    just cpp-install {{args}}
    just python-install

# Install sardine
cpp-install *args:
    conan create . -b missing {{args}}

# Install sardine-python
python-install *args:
    pip install .  {{args}}

# Install sardine in developer mode (editable) in conan and pip
dev *args:
    just cpp-dev {{args}}
    just python-dev

cpp-dev *args:
    conan editable add .
    conan build . -b missing {{args}}

python-dev *args:
    pip install --no-build-isolation -ve . {{args}}

build build_type="release":
    just cpp-build {{build_type}}
    just python-build

cpp-build build_type="release":
    cmake --build --preset "conan-{{build_type}}"

python-build:
    pip install --no-build-isolation -Ceditable.rebuild=true -ve .

# Run tests
test build_type="release":
    just cpp-test {{build_type}}

cpp-test build_type="release":
    ctest --preset conan-{{build_type}}

# Register sardine as editable in conan
@register:
    conan editable add .

# Unregister sardine as editable in conan
@unregister:
    conan editable remove .

@clean:
    just unregister
    rm -rf						        \
        build/*					        \
        CMakeUserPresets.json
