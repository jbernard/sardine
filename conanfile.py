from conan import ConanFile
from conan.tools.cmake import CMake, cmake_layout

class sardineConan(ConanFile):
    name = 'sardine'
    version = '1.0.0'
    license = ''

    exports_sources = 'CMakeLists.txt', 'include/*', 'src/*', 'test/*'

    settings = 'os', 'compiler', 'build_type', 'arch'

    options = {
        'fPIC'          : [True, False],
        'shared'        : [True, False],
    }

    default_options = {
        'fPIC'         : True,
        'shared'       : False,
        # 'emu/*:cuda'   : True,
    }

    def requirements(self):
        self.requires('emu/1.0.0', transitive_headers=True)
        self.requires('fmt/10.0.0', transitive_headers=True)
        self.requires('boost/1.84.0', transitive_headers=True)

        self.test_requires('gtest/1.13.0')

    def layout(self):
        cmake_layout(self)

    generators = 'CMakeDeps', 'CMakeToolchain'

    def build(self):
        cmake = CMake(self)

        cmake.configure()
        cmake.build()

        cmake.test()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ['sardine']
