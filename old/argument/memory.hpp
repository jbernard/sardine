#include <sardine/argument/base.hpp>

namespace sardine::argument::memory
{

    template <typename T>
    struct Argument final : Interface<T>
    {
        using impl_type = T;
        using base_type = T*;

        Argument(sardine::json j): s(as_span<T>(j))
        {}

        std::span<T> s;

        void recv(T &value) override
        {
            value = *s.data();
        }

        void send(const T &) override
        {
            // do nothing.
        }
    };

    template <typename T>
    std::unique_ptr<Interface<T>> instance(const url &u, sardine::json j)
    {
        return std::make_unique<Argument<T>>(j);
    }

} // namespace sardine::argument::memory
