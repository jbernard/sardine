#pragma once

#include <sardine/argument/base.hpp>

#include <sardine/shm/json.hpp>
#include <sardine/shm/managed/manager.hpp>

namespace sardine::argument::managed
{

    template<typename T>
    struct Argument : Interface<T>
    {
        Argument(T* ptr):ptr(ptr) {}

        T* ptr;

        void recv(T& t) override {
            t = *ptr;
        }

        void send(const T& t) override {
            *ptr = t;
        }

    };

    template <typename T>
    std::unique_ptr<Interface<T>> instance(const url &u, shm::json_t&)
    {
        auto& segment = shm::managed::manager::instance().at(u.path());

        T* ptr = segment.find_or_construct<T>(u.fragment().c_str())();


        return std::make_unique<Argument<T>>(ptr);
    }

} // namespace sardine::argument::managed
