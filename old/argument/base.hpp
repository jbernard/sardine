#ifndef SARDINE_ARGUMENT_BASE_HPP
#define SARDINE_ARGUMENT_BASE_HPP

#include <sardine/type/url.hpp>
#include <sardine/shm/json.hpp>

#include <memory>

namespace sardine::argument
{

    template <typename T>
    struct Interface
    {

        virtual ~Interface() = default;

        virtual void recv(T &) = 0;
        virtual void send(const T &) = 0;
    };

    template <typename T>
    std::unique_ptr<Interface<T>> instance(url u, shm::json_t patch);

    // template <typename T>
    // struct Argument;




} // namespace sardine::argument

#endif // SARDINE_ARGUMENT_BASE_HPP
