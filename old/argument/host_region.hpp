#include <sardine/shm/region/host.hpp>
#include <sardine/shm/region/host/manager.hpp>

#include <sardine/argument/base.hpp>

#include <span>

namespace sardine::argument::region::host
{

    template <typename T>
    struct Argument;

    template <typename T>
    struct Argument<std::span<T>> final : Interface<std::span<T>>
    {
        using impl_type = T;
        using base_type = T*;

        Argument(shm::region::host r, sardine::json j): s(as_span<T>(r, j))
        {}

        std::span<T> s;

        void recv(std::span<T> &value) override
        {
            value = s;
        }

        void send(const std::span<T> &) override
        {
            // do nothing.
        }
    };

    template <typename T>
    struct Argument<T*> final : Interface<T*>
    {
        using impl_type = T;
        using base_type = T*;

        Argument(sardine::host_region r, sardine::json j): s(as_span<T>(r, j))
        {}

        std::span<T> s;

        void recv(base_type &value) override
        {
            value = s.data();
        }

        void send(const base_type &) override
        {
            // do nothing.
        }
    };

    template <typename T>
    std::unique_ptr<Interface<T>> instance(const url &u, sardine::json j)
    {
        auto& mapped_region = shm::region::host::manager::instance().at(u.path());

        auto region = map(mapped_region);

        // url cu = u;

        // auto params = cu.params();

        std::size_t offset = 0;

        if (auto it = j.find("offset_byte"); it != j.object_range().end())
            offset = it->value().as<std::size_t>(); //std::stoul((*it).value);

        std::size_t size = region.size - offset;

        if (auto it = j.find("size_byte"); it != j.object_range().end())
            size = it->value().as<std::size_t>();

        region = region.subregion(offset, size);

        return std::make_unique<Argument<T>>(region, j);
    }


} // namespace sardine::argument::host_region
