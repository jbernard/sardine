#pragma once

#include <sardine/argument/base.hpp>
#include <sardine/type.hpp>
#include <sardine/shm/json.hpp>

#include <emu/optional.h>

#include <jsoncons_ext/mergepatch/mergepatch.hpp>

#include <algorithm>
#include <span>

namespace sardine::argument::json
{

namespace cpts
{
    template <typename T>
    concept json_parsable = requires(T value, shm::json_t json)
    {
        value = json.as<T>();
        json = jsoncons::json_type_traits<shm::json_t, T>::to_json(value, json.get_allocator());
    };

} // namespace cpts

    // template <typename T>
    // concept Isjson_parsable = json_type_traits<json, T>::is_compatible or not requires(){json_type_traits<json, T>::is_compatible;};



    template <typename T>
    struct Argument : Interface<T>
    {
        static constexpr bool custom_json = false;
        using impl_type = T;

        Argument(shm::json_t& json) : json(json)
        {
        }

        shm::json_t& json;

        void recv(T &value) override
        {
            value = json.as<T>();
        }

        void send(const T &value) override
        {
            json = jsoncons::json_type_traits<shm::json_t, T>::to_json(value, json.get_allocator());
        }
    };

    template <typename T>
    struct Argument<std::vector<T>> : Interface<std::vector<T>>
    {
        static constexpr bool custom_json = true;

        Argument(shm::json_t& json) : json(json)
        {
        }

        shm::json_t& json;

        void recv(std::vector<T>& value) override
        {
            // Avoid using simple assignment to avoid useless reallocation.
            value.resize(json.size());
            std::ranges::transform(json.array_range(), value.begin(), [](const auto &json) { return json.template as<T>(); });
        }

        void send(const std::vector<T>& value) override
        {
            // Avoid using simple assignment to avoid useless reallocation.
            json.resize(value.size());
            std::ranges::transform(value, json.array_range().begin(), [&](const auto &v) {
                return jsoncons::json_type_traits<shm::json_t, T>::to_json(v, json.get_allocator());
            });
        }
    };

    template <typename Span>
    requires std::is_base_of_v<std::span<typename Span::element_type>, Span>
    struct Argument<Span> : Interface<Span>
    {
        static constexpr bool custom_json = true;
        using impl_type = std::vector<typename Span::value_type>;

        Argument(shm::json_t& json) : impl(json)
        {}

        Argument<impl_type> impl;

        impl_type buffer;

        // value_type is the non const version of element_type.
        // std::vector<typename Span::value_type> buffer;

        void recv(Span &value) override
        {
            impl.recv(buffer);
            value = buffer;
        }

        void send(const Span &) override
        {
            impl.send(buffer);
        }
    };

    template<typename T>
    constexpr bool CustomJson = Argument<T>::custom_json;

    shm::json_t& from_url(const url &u);

    emu::optional<url> as_url(const shm::json_t &json);

    template <typename T>
    std::unique_ptr<Interface<T>> instance(const url &u, shm::json_t patch)
    {
        auto json = from_url(u);

        return as_url(json)     // try to extract url from json.
            .map([&](url new_u) {
                if constexpr (std::is_same_v<T, url>) // special case for url. If you want to pass a sardine url, pass it as an object.
                    return new_u;
                else
                    return argument::instance<T>(u, patch); }
            ) // if it's a url, parse it.
            .or_else([&] () -> emu::optional<std::unique_ptr<Interface<T>>> { // else,
                if (json.is_object() and json.contains("$url")) // if url have a $ prefix, it's a sardine url.
                {
                    auto url = as_url((json)["$url"]);
                    if (url) {
                        if (json.contains("parameter") and (json)["parameter"].is_object())
                        {
                            // TODO FIX, cannot copy parameter into j because not same type.
                            // Possible solution: decode + encode.
                            // Open a ticket on https://github.com/danielaparker/jsoncons/issues.
                            auto range = (json)["parameter"].object_range();
                            shm::json_t j(jsoncons::json_object_arg, range.begin(), range.end(), shm::managed::v_allocator<char>(shm::managed::heap_allocator));
                            jsoncons::mergepatch::apply_merge_patch(j, patch);

                            return argument::instance<T>(*url, j);
                        }
                        else
                            return argument::instance<T>(*url, patch);
                    }
                }
                return emu::nullopt;
            })
            .or_else([&] {              // else, get the required value.
                if constexpr (cpts::json_parsable<T> or CustomJson<T>)
                {
                    if (json.is<typename Argument<T>::impl_type>())
                        return std::make_unique<Argument<T>>(json);
                    else
                        throw std::runtime_error(fmt::format("{} from {} is not parsable as {}", json, u, type_name_v<T>));
                }
                else
                    throw std::runtime_error(fmt::format("Type {} not supported by json", type_name_v<T>));
            })
            .value();
    }

} // namespace sardine::argument::json
