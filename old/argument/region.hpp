#pragma once

#include <sardine/wrap/argument/base.hpp>
#include <sardine/detail/cuda_ipc.hpp>

#include <emu/misc/location.h>
#include <emu/cuda/misc/location.h>

namespace sardine::argument::region
{
    using host_mapped_region = boost::interprocess::mapped_region;
    using cuda_mapped_region = ipc::handle_t;

    cuda_mapped_region open(const std::string &name);
    cuda_mapped_region create(const std::string &name, std::size_t size, ::cuda::device_t id);
    cuda_mapped_region open_or_create(const std::string &name, std::size_t size, ::cuda::device_t id);




    emu::location::host_t location(const host_mapped_region &) {
        return {};
    }

    emu::location::cuda_t location(const cuda_mapped_region &region) {
        return emu::location::cuda_t(region.device_id());
    }

    template<typename T>
    auto map(const host_mapped_region &region) {
        return Memory<T, emu::location::host_t>{static_cast<T*>(region.get_address()), region.get_size() / sizeof(T), {}};
    }

    template<typename T>
    auto map(const cuda_mapped_region &region) {
        return Memory<T, emu::location::cuda_t>{static_cast<T*>(region.map()), region.size() / sizeof(T), region.device_id()};
    }

    template<typename T, typename Location>
    struct CircularBuffer
    {
        Memory<T, Location> memory;

        std::size_t buffer_nb;
        std::size_t buffer_offset;

        std::size_t pos;
        std::size_t *global_pos;
    };

    template <typename T>
    struct Argument;


    template <typename Span>
    requires std::is_base_of_v<std::span<typename Span::element_type>, Span>
    struct Argument<Span> final : Interface<Span>
    {
        using impl_type = T;

        Argument(const url &u) : u(u)
        {
        }

        mapped_region region;

        void recv(Span &value) override
        {
            throw std::runtime_error("Not implemented");
        }

        void send(const T &value) override
        {
            throw std::runtime_error("Not implemented");
        }
    };

    template <typename T>
    struct Arguement<UpdatableData<T>> final : Interface<UpdatableData<T>>
    {
        using impl_type = T;

        Argument(const url &u) : u(u)
        {}

        mapped_region region;

        void recv(UpdatableData<T> &value) override
        {
            throw std::runtime_error("Not implemented");
        }

        void send(const T &value) override
        {
            if (value.updated)
            {
                throw std::runtime_error("Not implemented");
            }
        }
    };

    mapped_region from_url(const url &u);

    template <typename T>
    std::unique_ptr<Interface<T>> instance(const url &u)
    {
        auto region = from_url(u);




        throw std::runtime_error("Not implemented");
    }

} // namespace sardine::argument::region