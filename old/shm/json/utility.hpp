#pragma once

#include <sardine/shm/managed/allocator.hpp>
#include <sardine/shm/managed.hpp>

#include <emu/type_traits.h>
#include <emu/optional.h>

#include <jsoncons/json.hpp>
#include <jsoncons/pretty_print.hpp>

#include <fmt/format.h>
#include <fmt/ostream.h>

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <boost/interprocess/containers/vector.hpp>

#include <string_view>
#include <scoped_allocator>

namespace sardine::shm
{

namespace json
{

    struct sorted_policy
    {
        template <class KeyT, class Json>
        using object = jsoncons::sorted_json_object<KeyT, Json, boost::interprocess::vector>;

        template <class Json>
        using array = jsoncons::json_array<Json, boost::interprocess::vector>;

        template <class CharT, class CharTraits, class Allocator>
        using member_key = boost::interprocess::basic_string<CharT, CharTraits, Allocator>;
    };

    using shm_allocator = managed::v_allocator<char>;

    using allocator = std::scoped_allocator_adaptor<shm_allocator>;

    using impl_t = jsoncons::basic_json<char, sorted_policy, allocator>;

} // namespace json

    struct json_t {
        struct maybe_delete {
            void operator()(json::impl_t* impl) const {
                if (do_delete) {
                    delete impl;
                }
            }

            bool do_delete = true;
        };

        std::unique_ptr<json::impl_t, maybe_delete> impl;
        json::allocator alloc;

        json_t(json::impl_t* impl, json::allocator alloc)
            : impl(impl, maybe_delete{false})
            , alloc(alloc)
        {}

        json_t(json::impl_t impl, json::allocator alloc)
            : impl(new json::impl_t(std::move(impl)), maybe_delete{true})
            , alloc(alloc)
        {}

        json::impl_t& operator*() const { return *impl; }
        json::impl_t* operator->() const { return impl.get(); }

    };

namespace cpts
{

    template<typename T>
    concept json = emu::is_specialization<std::decay_t<T>, jsoncons::basic_json> or emu::cpts::equivalent<T, json_t>;

} // namespace cpts

    // template <typename T, cpts::json Json>
    // emu::optional<T> try_get(Json&& j, std::string_view key)
    // {
    //     if (auto it = j.find(key); it != j.object_range().end())
    //         return it->value().template as<T>();
    //     else
    //         return emu::nullopt;
    // }

namespace json
{

    inline json_t empty() {
        return {jsoncons::null_type(), shm_allocator(heap_allocator)};
    }

} // namespace json

} // namespace sardine::shm

namespace sardine::shm::managed::spe
{

    template<>
    struct managed_adaptor<shm::json_t>
    {

        static auto create(shared_memory& shm, char_ptr_holder_t name) -> shm::json_t {
            auto& seg = *shm.get_segment_manager();

            shm::json::allocator alloc{shm::json::shm_allocator(&seg)};

            return std::move(shm::json_t(seg.construct<shm::json::impl_t>(name)(jsoncons::json_object_arg, alloc), alloc));
        }

        static auto exist(shared_memory& shm, char_ptr_holder_t name) -> bool {
            auto& seg = *shm.get_segment_manager();
            return seg.find<shm::json::impl_t>(name).first != 0;
        }

        static auto destroy(shared_memory& shm, char_ptr_holder_t name) -> bool {
            auto& seg = *shm.get_segment_manager();
            return seg.destroy<shm::json::impl_t>(name);
        }

        static auto from_address(shared_memory& shm, std::byte* address) -> shm::json_t {
            auto& seg = *shm.get_segment_manager();

            shm::json::allocator alloc{shm::json::shm_allocator(&seg)};

            return std::move(shm::json_t(reinterpret_cast<shm::json::impl_t*>(address), alloc));
        }

        static auto address_of(const shm::json_t& value) -> const std::byte* {
            return reinterpret_cast<std::byte*>(&(*value));
        }

    };

} // namespace sardine::shm::managed::spe

template <typename Json, typename CharT>
struct fmt::formatter<jsoncons::json_printable<Json>, CharT> : fmt::ostream_formatter {};

template <typename CharT, typename ImplementationPolicy, typename Allocator>
struct fmt::formatter<jsoncons::basic_json<CharT, ImplementationPolicy, Allocator>, CharT>
    : fmt::formatter<jsoncons::json_printable<jsoncons::basic_json<CharT, ImplementationPolicy, Allocator>>, CharT>
{
    using json = jsoncons::basic_json<CharT, ImplementationPolicy, Allocator>;

    using base = fmt::formatter<jsoncons::json_printable<json>>;

    template <typename FormatContext>
    auto format(const json &j, FormatContext &ctx) const
    {
        return base::format(jsoncons::pretty_print(j), ctx);
    }
};

template <typename CharT>
struct fmt::formatter<sardine::shm::json_t, CharT> : fmt::formatter<sardine::shm::json::impl_t, CharT>
{
    using base = fmt::formatter<sardine::shm::json::impl_t, CharT>;

    template <typename FormatContext>
    auto format(const sardine::shm::json_t &j, FormatContext &ctx) const
    {
        return base::format(*j, ctx);
    }
};