#pragma once

#include <sardine/shm/json.hpp>

#include <string>
#include <string_view>
#include <unordered_map>

namespace sardine::shm::json
{

    struct manager : protected std::unordered_map<std::string, json_t*>
    {
        using base = std::unordered_map<std::string, json_t*>;

        static manager& instance();

    private:
        manager() = default;

    public:
        manager(const manager&) = delete;
        manager(manager&&) = delete;

        json_t& open(std::string name);
        json_t& create(std::string name, std::size_t file_size);
        json_t& open_or_create(std::string name, std::size_t file_size);

        json_t& at(const std::string& name);
        // json_t& at(std::string_view name);
    };

} // namespace sardine::shm::json
