#pragma once

#include <sardine/type/url.hpp>

#include <jsoncons/json.hpp>

#include <sardine/shm/json/utility.hpp>

namespace sardine::shm::json
{

    // json_t open(const std::string& name);
    // json_t create(const std::string& name, std::size_t file_size);
    // json_t open_or_create(const std::string& name, std::size_t file_size);

    url url_of(const shm::json_t &json);

    template<typename T>
    shm::json_t from_url(const url &u) {
        auto json = managed::from_url<shm::json_t>(u);

        try {
            return shm::json_t(&jsoncons::jsonpointer::get(*json, u.fragment()), json.alloc);
        } catch(const jsoncons::jsonpointer::jsonpointer_error&) {
            throw std::runtime_error(fmt::format("json {} at {} don't have attribute at {}", json, u.path(), u.fragment()));
        }
    }


} // namespace sardine::shm::json
