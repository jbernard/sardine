#pragma once

#include <sardine/type/url.hpp>
#include <sardine/shm/managed/utility.hpp>
#include <sardine/shm/managed/manager.hpp>

#include <emu/cstring_view.hpp>
#include <emu/optional.h>
#include <emu/utility.h>

#include <boost/lexical_cast.hpp>

#include <future>

namespace sardine::shm
{

    struct managed_t
    {
        using char_ptr_holder_t = managed::char_ptr_holder_t;

        managed::shared_memory* shm_;
        // std::string name;

        managed_t(managed::shared_memory* shm) //, std::string name)
            : shm_{shm}
            // , name(std::move(name))
        {}

        template<typename Fn>
        auto atomic_call(Fn fn) -> decltype(auto) {
            using ret_type = decltype(fn());

            // Would have be great to use std::optional but does not allow reference.
            std::promise<ret_type> p;

            // Cannot retrieve the return value from atomic_func, so we use a promise to pass back the value.
            // atomic_func only takes function reference, function need to be a lvalue.
            auto l = [&]{ p.set_value(fn()); };
            shm().atomic_func(l);

            auto f = p.get_future();
            return f.get();
        }

        template<typename T, typename... Args>
        auto force_create(char_ptr_holder_t name, Args&&... args) -> decltype(auto) {
            return atomic_call([&] () -> decltype(auto) {
                destroy<T>(name); // no need to check if exist, destroy will do nothing if not exist.
                return create_unchecked<T>(name, EMU_FWD(args)...);
            });

        }

        template<typename T, typename... Args>
        auto create(char_ptr_holder_t name, Args&&... args) -> decltype(auto) {
            return atomic_call([&] () -> decltype(auto) {
                if (!exist<T>(name))
                    return create_unchecked<T>(name, EMU_FWD(args)...);
                else
                    throw std::runtime_error(fmt::format("{} {} already exist", type_name_v<T>, name));
            });
        }

        template<typename T, typename... Args>
        auto create_unchecked(char_ptr_holder_t name, Args&&... args) -> decltype(auto) {
            return managed::spe::managed_adaptor<T>::create(shm(), name, EMU_FWD(args)...);
        }

        template<typename T>
        auto open_unchecked(char_ptr_holder_t name) -> decltype(auto) {
            auto& seg = *shm().get_segment_manager();
            auto* address = seg.find<std::byte>(name).first;
            return managed::spe::managed_adaptor<T>::from_address(shm(), address);
        }

        template<typename T>
        auto open(char_ptr_holder_t name) -> decltype(auto) {
            return atomic_call([&] () -> decltype(auto) {
                if (exist<T>(name))
                    return open_unchecked<T>(name);
                else
                    throw std::runtime_error(fmt::format("{} {} not found", type_name_v<T>, name));
            });
        }

        template<typename T, typename... Args>
        auto open_or_create(char_ptr_holder_t name, Args&&... args) -> decltype(auto) {
            return atomic_call([&] () -> decltype(auto) {
                if (exist<T>(name))
                    return open_unchecked<T>(name);
                else
                    return create_unchecked<T>(name, EMU_FWD(args)...);
            });
        }

        template<typename T>
        bool type_check(char_ptr_holder_t name) {
            // TODO: check named_range to see if nothing exist for real and detect type mismatch.
            // HOW ??? Damn you past julien...
            return true;
        }

        template<typename T>
        bool exist(char_ptr_holder_t name) {
            return managed::spe::managed_adaptor<T>::exist(shm(), name);
        }

        template<typename T>
        bool destroy(char_ptr_holder_t name) {
            return managed::spe::managed_adaptor<T>::destroy(shm(), name);
        }

        managed::shared_memory& shm() const {
            return *shm_;
        }

        managed::named_range named() const {
            return managed::named_range(shm_);
        }

        template<typename T>
        managed::allocator<T> get_allocator() const {
            return {shm_->get_segment_manager()};
        }

        template<typename T>
        auto offset_of(const T& value) const -> decltype(auto) {
            auto addr = managed::spe::managed_adaptor<T>::address_of(shm(), value);
            return shm().get_handle_from_address(addr);
        }

        template<typename T>
        auto from_offset(managed::handle_t offset) const -> decltype(auto) {
            std::byte* address = reinterpret_cast<std::byte*>(shm_->get_address_from_handle(offset));
            return managed::spe::managed_adaptor<T>::from_address(shm(), address);
        }



    };

namespace managed
{

    inline managed_t open(std::string name) {
        return {&manager::instance().open(name.c_str()) /*, std::move(name)*/};
    }

    inline managed_t create(std::string name, std::size_t file_size) {
        return {&manager::instance().create(name.c_str(), file_size)/* , std::move(name) */};
    }

    inline managed_t open_or_create(std::string name, std::size_t file_size) {
        return {&manager::instance().open_or_create(name.c_str(), file_size)/* , std::move(name) */};
    }

    template<typename T>
    inline emu::optional<sardine::url> url_of(const T& value) {
        auto addr = spe::managed_adaptor<T>::address_of(value);

        auto handle = manager::instance().find_handle(addr);

        if (handle) {

            // try find if a name correspond to the handle offset.
            auto shm = managed_t(&manager::instance().at(handle->name)/* , std::string(handle->name) */);
            auto named = shm.named();

            if (auto it = std::ranges::find(named, addr, &named_value_t::value); it != named.end())
                return sardine::url(fmt::format("shm://{}/{}", handle->name, it->name()));
            else
                return sardine::url(fmt::format("shm://{}/+{}", handle->name, handle->offset));
        }
        else
            return emu::nullopt;
    }

    template<typename T>
    inline auto from_url(const sardine::url& url) -> decltype(auto) {
        auto shm = shm::managed::open(url.host());

        auto path = url.path().substr(1);

        if (path.size() <= 1)
            throw std::runtime_error(fmt::format("Invalid url {}", url));

        if (path[0] == '+') {
            auto offset = boost::lexical_cast<std::size_t>(path.substr(1));
            return shm.from_offset<T>(offset);
        }
        else
            return shm.open<T>(path.c_str());
    }

} // namespace managed


} // namespace sardine::shm
