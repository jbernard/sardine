#pragma once

#include <sardine/shm/semaphore.hpp>

#include <string>
#include <unordered_map>

namespace sardine::shm::semaphore
{

    struct manager : protected std::unordered_map<std::string, semaphore_t>
    {
        using base = std::unordered_map<std::string, semaphore_t>;

        static manager &instance();

    private:
        manager() = default;

    public:
        manager(const manager &) = delete;
        manager(manager &&) = delete;

        semaphore_t& open(std::string name);
        semaphore_t& create(std::string name, unsigned int initial_count);
        semaphore_t& open_or_create(std::string name, unsigned int initial_count);

        semaphore_t& at(const std::string& name);
    };


} // namespace sardine::shm::semaphore
