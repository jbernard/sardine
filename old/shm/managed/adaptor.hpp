#pragma once

#include <sardine/type/managed.hpp>

namespace sardine::managed
{

namespace spe
{

    template<typename T>
    struct default_managed_adaptor
    {

        template<typename... Args>
        static auto create(shared_memory& shm, char_ptr_holder_t name, Args&&... args) -> decltype(auto) {
            auto& seg = *shm.get_segment_manager();
            allocator<std::byte> alloc(&seg);

            return std::apply([&](auto&&... args) -> decltype(auto) {
                return *seg.construct<T>(name)(std::forward<decltype(args)>(args)...);
            }, std::uses_allocator_construction_args<T>(alloc, std::forward<Args>(args)...));
        }

        static auto exist(shared_memory& shm, char_ptr_holder_t name) -> bool {
            auto& seg = *shm.get_segment_manager();
            return seg.find<T>(name).first != 0;
        }

        static auto destroy(shared_memory& shm, char_ptr_holder_t name) -> bool {
            auto& seg = *shm.get_segment_manager();
            return seg.destroy<T>(name);
        }

        static auto from_address(shared_memory&, std::byte* address) -> decltype(auto) {
            return *reinterpret_cast<T*>(address);
        }

        static auto address_of(const T& value) -> const std::byte* {
            return reinterpret_cast<const std::byte*>(&value);
        }

    };

    template<typename T>
    struct managed_adaptor : default_managed_adaptor<T>
    {};

} // namespace spe

} // namespace sardine::managed
