#pragma once

#include <sardine/type.hpp>

#include <emu/utility.h>

#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <boost/interprocess/containers/map.hpp>

#include <emu/cstring_view.hpp>
#include <emu/optional.h>

#include <fmt/format.h>

#include <map>
#include <ranges>
#include <memory>
#include <functional>

namespace sardine::shm::managed
{

    using shared_memory = boost::interprocess::managed_shared_memory;
    using segment_manager = boost::interprocess::managed_shared_memory::segment_manager;

    using char_ptr_holder_t = segment_manager::char_ptr_holder_t;

    using handle_t = shared_memory::handle_t;

    struct named_handle {
        emu::cstring_view name;
        handle_t offset;
    };

    template <typename T>
    using allocator = boost::interprocess::allocator<T, segment_manager>;

    using string = boost::interprocess::basic_string<char, std::char_traits<char>, allocator<char>>;

    template <typename T>
    using vector = boost::interprocess::vector<T, allocator<T>>;

    // We choose to differ from std::map and user std::less<> as default. This is because
    // we want to be able to use a different type as key than the one used in the map for lookup.
    template <typename Key, typename T, typename Compare = std::less<>>
    using map = boost::interprocess::map<Key, T, Compare, allocator<std::pair<const Key, T>>>;

    struct manager;

    struct named_range : public std::ranges::view_interface<named_range>
    {

        shared_memory* shm_;

        named_range(shared_memory* shm)
            : shm_{shm}
        {}

        auto begin() const {
            return shm_->named_begin();
        }

        auto end() const {
            return shm_->named_end();
        }

        auto size() const {
            return shm_->get_num_named_objects();
        }
    };

    using named_value_t = std::ranges::range_value_t<named_range>;

namespace spe
{

    template<typename T>
    struct default_managed_adaptor
    {

        template<typename... Args>
        static auto create(shared_memory& shm, char_ptr_holder_t name, Args&&... args) -> decltype(auto) {
            auto& seg = *shm.get_segment_manager();
            allocator<std::byte> alloc(&seg);

            return std::apply([&](auto&&... args) -> decltype(auto) {
                return *seg.construct<T>(name)(std::forward<decltype(args)>(args)...);
            }, std::uses_allocator_construction_args<T>(alloc, std::forward<Args>(args)...));
        }

        static auto exist(shared_memory& shm, char_ptr_holder_t name) -> bool {
            auto& seg = *shm.get_segment_manager();
            return seg.find<T>(name).first != 0;
        }

        static auto destroy(shared_memory& shm, char_ptr_holder_t name) -> bool {
            auto& seg = *shm.get_segment_manager();
            return seg.destroy<T>(name);
        }

        static auto from_address(shared_memory&, std::byte* address) -> decltype(auto) {
            return *reinterpret_cast<T*>(address);
        }

        static auto address_of(const T& value) -> const std::byte* {
            return reinterpret_cast<const std::byte*>(&value);
        }

    };

    template<typename T>
    struct managed_adaptor : default_managed_adaptor<T>
    {};

} // namespace spe


} // namespace sardine::shm::managed

namespace boost::interprocess::ipcdetail
{

    template<typename CharT>
    const CharT* format_as(char_ptr_holder<CharT> name) {
        if (name.is_unique())
            return "unique";
        else if (name.is_anonymous())
            return "anonymous";
        else
            return name.get();
    }

} // namespace boost::interprocess::ipcdetail

template<typename CharT, typename Traits, typename Allocator>
struct fmt::formatter<boost::container::basic_string<CharT, Traits, Allocator>> : fmt::formatter<const CharT*> {
    template <typename FormatContext>
    auto format(boost::container::basic_string<CharT, Traits, Allocator> const &data, FormatContext &ctx) const {
        return fmt::formatter<const CharT*>::format(data.c_str(), ctx);
    }
};
