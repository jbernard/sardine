#pragma once

#include <sardine/value/base.hpp>
#include <sardine/value/json.hpp>
#include <sardine/value/managed.hpp>
#include <sardine/value/region/host.hpp>

namespace sardine
{

    template<typename T>
    result<value<T>> value<T>::open_impl(url_view u, const map_t& patch) {
        auto scheme = u.scheme();

        if (scheme == json::url_scheme) {
            return json::open<T>(u, patch);
        } else if (scheme == managed::url_scheme) {
            return managed::open<T>(u, patch);
        } else
            return emu::unexpected(fmt::format("scheme {} not supported", scheme));
    }

    template<typename T>
    struct impl : value::interface<T>
    {
        impl(producer<T> prod, comsumer<T> cons)
            : prod(std::move(prod))
            , cons(std::move(cons))
        {}

        producer<T> prod;
        comsumer<T> cons;

        void recv(T& t) override {
            cons.recv(t);
        }

        void send(const T& t) override {
            prod.send(t);
        }

    };

    template<typename T>
    result<value<T>> value<T>::wrap_buffer_to_value(url_view u, const map_t& patch) {
        auto prod = producer<T>::open(u, patch);
        auto cons = consumer<T>::open(u, patch);

        if (prod && cons) {
            return std::make_unique<impl<T>>(std::move(*prod), std::move(*cons));
        } else
            return emu::unexpected("could not open producer and consumer");
    }

    template<typename T>
    result<value<T>> value<T>::open(url_view u, const map_t& patch) {
        return open_impl(u, patch)
                .or_else([&] {
                    return wrap_buffer_to_value(u, patch); // return a result<value<T>>
                });
            //TODO: we should be able to detect two situations: url not found and invalid url (with more informations)
    }

} // namespace sardine

template<typename T, typename CharT>
struct fmt::formatter<sardine::value<T>, CharT> {

    constexpr auto parse(format_parse_context& ctx) {
        return ctx.begin();
    }

    template<typename FormatContext>
    auto format(const sardine::value<T>& value, FormatContext& ctx) {
        return format_to(ctx.out(), "value({})", *value);
    }
};