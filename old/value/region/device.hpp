#pragma once

#include <sardine/value/base.hpp>
#include <sardine/region/device.hpp>

#include <emu/concepts.hpp>

namespace sardine::region::cuda
{
    template<typename T>
    struct impl;

    template<emu::cpts::span Span>
    struct impl< Span > : value::interface< Span >
    {
        using value_type = Span;

        impl(value_type data): data(data) {}

        value_type data;

        void recv(value_type& t) override {
            t = data;
        }

        void send(const value_type& t) override {
        }

    };

    template <typename T>
    value::interface_ptr<T> open(url u, const json_t& patch) {
        if constexpr ( emu::cpts::device_span<T> ) {
            auto data = cuda::from_url<typename T::element_type>(u);

            return std::make_unique<impl<decltype(data)>>(std::move(data));
        } else
            throw std::runtime_error(fmt::format("{} not handle by device shm", type_name_v<T>));
    }


} // namespace sardine::region::cuda