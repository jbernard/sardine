#pragma once

#include <sardine/value/base.hpp>

namespace sardine::managed
{

    template<typename T>
    struct impl : value::interface<T>
    {
        impl(T value, const json_t&): value(std::move(value)) {}

        T value;

        void recv(T& t) override {
            t = value;
        }

        void send(const T& t) override {
            value = t;
        }

    };

    template<typename T>
    struct impl<T&> : value::interface<T>
    {
        impl(T& ref, const json_t&):ptr(&ref) {}

        T* ptr;

        void recv(T& t) override {
            t = *ptr;
        }

        void send(const T& t) override {
            *ptr = t;
        }

    };

    template <typename T>
    value::interface_ptr<T> open(url u, const json_t& patch) {
        decltype(auto) value = managed::from_url<T>(u);

        return std::make_unique<impl<decltype(value)>>(EMU_FWD(value), patch);
    }

} // namespace sardine::managed
