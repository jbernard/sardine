#pragma once

#include <sardine/type/url.hpp>
#include <sardine/json.hpp>
#include <sardine/type.hpp>

#include <memory>

namespace sardine
{
    using map_t = json_t;

    template<typename T>
    struct value {

        struct interface
        {
            virtual void recv(T &value) = 0;
            virtual void send(const T &value) = 0;
        };

        std::unique_ptr<interface> impl;
        T data;

        void recv() {
            impl->recv(data);
        }

        void send() {
            impl->send(data);
        }

        T& operator*() noexcept {
            return data;
        }

        const T& operator*() const noexcept {
            return data;
        }

        T* operator->() noexcept {
            return &data;
        }

        const T* operator->() const noexcept {
            return &data;
        }

        static result<value> open(url_view u, const map_t& patch);

    };

} // namespace sardine


// #pragma once

// #include <sardine/type/url.hpp>
// #include <sardine/json.hpp>

// #include <memory>

// namespace sardine
// {
//     using Map = json_t;

// namespace value
// {


//     template<typename T>
//     struct interface
//     {
//         virtual void recv(T &value) = 0;
//         virtual void send(const T &value) = 0;
//     };

//     template<typename T>
//     using interface_ptr = std::unique_ptr<interface<T>>;

//     template<typename T>
//     interface_ptr<T> open(url u, const Map& patch);

// } // namespace value

// } // namespace sardine
