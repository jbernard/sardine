#pragma once

#include <sardine/value/base.hpp>

#include <sardine/json.hpp>

#include <emu/concepts.hpp>
#include <emu/span.hpp>

#include <memory>
#include <vector>

namespace sardine
{

    // template<cpts::json Json>
    // struct spe::url_adaptor<Json> : spe::default_url_adaptor<json::impl_t>
    // {
    //     using base = url_adaptor<json::impl_t>;

    //     static url url_of(const Json& json) {
    //         // only the impl part is located in the shared memory.
    //         // It is used to create the url.
    //         return base::url_of(*json.impl);
    //     }

    //     static auto from_url(url_view url) -> decltype(auto) {
    //         // On the other hand,
    //         return managed::from_url<Json>(url);
    //     }

    // };

namespace json
{

    template <typename T>
    struct impl : value::interface<T>
    {
        static constexpr bool default_ = true;
        using impl_type = T;

        impl(json_t json, const json_t&) : json(std::move(json))
        {}

        json_t json;

        void recv(T &value) override
        {
            value = json.as<T>();
        }

        void send(const T &value) override
        {
            json = value;
        }
    };

    template <typename T>
    struct impl<std::vector<T>> : value::interface<std::vector<T>>
    {
        using impl_type = std::vector<T>;

        impl(json_t json, const json_t&) : json(std::move(json)) {}

        json_t json;

        void recv(std::vector<T>& value) override
        {
            // Avoid using simple assignment to avoid useless reallocation.
            value.resize(json->size());
            std::ranges::transform(json->array_range(), value.begin(), [](const auto &json) { return json->template as<T>(); });
        }

        void send(const std::vector<T>& value) override
        {
            // Avoid using simple assignment to avoid useless reallocation.
            json->resize(value.size());
            std::ranges::transform(value, json->array_range().begin(), [&](const auto &v) {
                return jsoncons::json_type_traits<sardine::json::impl_t, T>::to_json(v, json.alloc);
            });
        }
    };

    template <emu::cpts::span Span>
    struct impl<Span> : value::interface<Span>
    {
        using impl_type = std::vector<typename Span::value_type>;

        impl(json_t json, const json_t&) : impl_(std::move(json)) {}

        impl<impl_type> impl_;

        impl_type buffer;

        // value_type is the non const version of element_type.
        // std::vector<typename Span::value_type> buffer;

        void recv(Span &value) override
        {
            impl_.recv(buffer);
            value = buffer;
        }

        void send(const Span &) override
        {
            impl_.send(buffer);
        }
    };

namespace cpts
{

    template<typename T>
    concept is_default = impl<T>::default_ == true;

    template<typename T>
    concept is_custom = not is_default<T>; // TODO check if could be done inline (without is_default)

} // namespace cpts

    emu::optional<url> as_url(const json_t &json);

    // json_t apply_patch(json_t m, const json_t& patch);

    template <typename T>
    value::interface_ptr<T> open(json_t m, const json_t &patch) {
        if constexpr (std::is_same_v<T, url>) {
        // if T is url, then just wrapped json.
            return std::make_unique<impl<T>>(m);
        } else {
        // if T is not url, then try to open url.
            return as_url(m) // try to cast to url
                .map([&](url u) {
                    // open it with current patch.
                    return value::open<T>(u, patch);
                })
                .or_else([&] () -> emu::optional<value::interface_ptr<T>> {
                    // if json is an object and contains "$url" key, then open it with current patch.
                    if (m->is_object() and m->contains("$url")) {
                        return as_url(m.at("$url")).map([&](url u) {
                            // Check if json contains parameter key.
                            if (m->contains("parameter") and m.at("parameter")->is_object())
                                return value::open<T>(u, apply_patch(m.at("parameter"), patch));
                            else
                                return value::open<T>(u, patch);
                        });
                    } else if constexpr (sardine::cpts::json_parsable<T> or cpts::is_custom<T>) {
                        if (m->is<typename impl<T>::impl_type>())
                            return std::make_unique<impl<T>>(std::move(m), patch);
                        else
                            throw std::runtime_error(fmt::format("{} is not parsable as {}", *m, type_name_v<T>));
                    } else
                        throw std::runtime_error(fmt::format("Type {} not supported by json", type_name_v<T>));
                }
            ).value();
        }
    }

    template <typename T>
    value::interface_ptr<T> open(url u, const json_t& patch) {
        return open<T>(sardine::json::from_url<json_t>(u), patch);
    }

} // namespace json

} // namespace sardine
