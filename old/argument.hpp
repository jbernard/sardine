#pragma once

#include <sardine/type/url.hpp>
#include <sardine/utility.hpp>

#include <sardine/argument/base.hpp>
#include <sardine/argument/json.hpp>
#include <sardine/argument/managed_shm.hpp>
#include <sardine/argument/host_region.hpp>
#include <sardine/argument/memory.hpp>

#include <jsoncons_ext/mergepatch/mergepatch.hpp>

#include <stdexcept>
#include <memory>

namespace sardine
{

namespace argument
{

    template<typename T>
    std::unique_ptr<Interface<T>> instance(url u, sardine::json patch) {
        auto scheme = u.scheme();

        sardine::json j(jsoncons::json_object_arg, jsoncons::semantic_tag::none);
        for (const auto& [key, value, has_value] : u.params()) {
            if (has_value)
                j[key] = value;
            else
                j[key] = true;
        }

        jsoncons::mergepatch::apply_merge_patch(j, patch);

        if      (scheme == "fps")
            throw std::runtime_error(fmt::format("scheme fps not implemented yet."));
        else if (scheme == "cacao")
            throw std::runtime_error(fmt::format("scheme cacao not implemented yet."));
        else if (scheme == "shmboost")
            return shm::instance<T>(std::move(u), j);
        else if (scheme == "shmjson")
            return json::instance<T>(std::move(u), j);
        else if (scheme == "hostregion")
            return host_region::instance<T>(std::move(u), j);
        // else if (scheme == "memory")
        //     return memory::instance<T>(std::move(u), j);
        else
            throw std::runtime_error(fmt::format("scheme {} not handled.", scheme));
    }

} // namespace argument

    template<typename T>
    struct Argument
    {
        std::unique_ptr<argument::Interface<T>> impl;

        T value;

        Argument(url u, json j):impl(argument::instance<T>(std::move(u), j)), value() {
            recv();
        }

        void recv() {
            impl->recv(value);
        }

        void send() {
            impl->send(value);
        }
    };

    template<typename T>
    struct Argument<emu::optional_t<T>>
    {
        std::unique_ptr<argument::Interface<T>> impl;

        emu::optional_t<T> value;

        Argument(url u, json j): impl(nullptr), value() {
            try {
                impl = argument::instance<T>(u, j);
            } catch (...) {}
            recv();
        }

        void recv() {
            if (impl)
                impl->recv(*value);
        }

        void send() {
            if (impl)
                impl->send(*value);
        }
    };



    template<typename T>
    struct Argument<UpdatableData<T>>
    {
        std::unique_ptr<argument::Interface<T>> impl;

        UpdatableData<T> value;

        Argument(url u, json j):impl(argument::instance<T>(u, j)), value() {
            recv();
        }

        void recv() {
            impl->recv(value.data);
        }

        void send() {
            if (value.updated)
                impl->send(value.data);
        }
    };

namespace argument
{

    template<typename T>
    auto open(url u) {
        auto j = sardine::json(jsoncons::json_object_arg, jsoncons::semantic_tag::none);
        return Argument<T>(std::move(u), j);
    }

    template<typename T>
    auto open(url u, sardine::json j) {
        return Argument<T>(std::move(u), j);
    }

} // namespace argument

} // namespace sardine
