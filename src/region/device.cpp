#include <sardine/region/device.hpp>
#include <sardine/region/device/manager.hpp>

#include <boost/lexical_cast.hpp>

namespace sardine::region::cuda
{

    view open(std::string name) {
        return manager::instance().open(name);
    }

    view create(std::string name, std::size_t size, ::cuda::device_t device) {
        return manager::instance().create(name, size, device);
    }

    view open_or_create(std::string name, std::size_t size, ::cuda::device_t device) {
        throw std::runtime_error("open_or_create is not supported for device regions.");
        // return manager::instance().open_or_create(name, size, device);
    }

    emu::optional<shm_handle> find_handle(const std::byte* ptr) {
        return manager::instance().find_handle(ptr);
    }

namespace detail
{

    emu::optional<url> url_of(emu::cuda::span<const std::byte> data) {
        return find_handle(data.data()).map([&](shm_handle handle) {

            return sardine::url(fmt::format("{}://{}/{}/{}", url_scheme, handle.name, handle.offset, data.size()));
        });
    }

    view from_url(url_view url) {
        auto data = open(url.host());

        auto path = url.path();

        // get the path and ignore the first '/'
        auto path_v = std::string_view(path).substr(1);

        if (path_v.size() <= 1)
            throw std::runtime_error(fmt::format("Invalid url {}: path {} is invalid", url, path_v));

        auto slash_count = std::ranges::count(path_v, '/');
        if (slash_count != 1)
            throw std::runtime_error(fmt::format("Invalid url {}: path {} should only contains 2 elements in path", url, path_v));

        auto slash_pos = path_v.find('/');
        if (slash_pos == std::string::npos)
            throw std::runtime_error(fmt::format("Invalid url {}: path {} should only contains 2 elements in path", url, path_v));

        auto offset = boost::lexical_cast<std::size_t>(path_v.substr(0, slash_pos));
        if (offset > data.size())
            throw std::runtime_error(fmt::format("Invalid url {}: offset {} is out of range, limit is {}", url, offset, data.size()));

        auto size = boost::lexical_cast<std::size_t>(path_v.substr(slash_pos + 1));
        if (offset + size > data.size())
            throw std::runtime_error(fmt::format("Invalid url {}: size {} is out of range, limit is {}", url, size, data.size() - offset));

        return data.subspan(offset, size);
    }

} // namespace detail

} // namespace sardine::region::cuda
