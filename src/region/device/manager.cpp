#include <sardine/region/device/manager.hpp>

#include <sardine/region/host/manager.hpp>
#include <sardine/utility.hpp>

#include <fmt/format.h>

namespace sardine::region::cuda
{


    manager &manager::instance()
    {
        static manager instance;
        return instance;
    }

    view_b manager::open(std::string name) {
        return find_or_emplace(static_cast<base&>(*this), name, [&name] {
            // open the shm object.
            auto host_view = host::open(name);

            if (host_view.size() < sizeof(handle_impl)) {
                throw std::runtime_error(fmt::format("The shared memory object {} is too small to contain a cuda handle.", name));
            }

            // Get the object address in shm.
            auto& object = *reinterpret_cast<handle_impl*>(host_view.data());

            // map the cuda ipc handle into the current process.
            return handle(object, /* owning = */ true);
        })->second.data;
    }

    view_b manager::create(std::string name, std::size_t size, ::cuda::device_t device) {
        return emplace_or_throw(static_cast<base&>(*this), name, [&] {
            // create the shm object.
            auto host_view = host::create(name, sizeof(handle_impl));

            // allocate memory on the device locally and put it in a pool to keep it alive.
            // TODO: try to use cuda-api-wrappers ipc memory instead.
            std::byte* ptr = pool.emplace(::cuda::memory::device::make_unique<std::byte[]>(device, size)).first->get();

            // create the cuda ipc handle.
            cudaIpcMemHandle_t cuda_handle;
            auto status = cudaIpcGetMemHandle(&cuda_handle, ptr);
            if (status != cudaSuccess) {
                throw std::runtime_error(fmt::format("Failed to get cuda ipc handle: {}", cudaGetErrorString(status)));
            }

            // Get the object address in shm and construct it.
            // No need to call the destructor when the shm object will be destroyed because handle_impl is trivially destructible.
            static_assert(std::is_trivially_destructible_v<handle_impl>, "handle_impl must be trivially destructible or manager needs to call the destructor explicitly.");
            auto& object = *new (reinterpret_cast<handle_impl*>(host_view.data())) handle_impl{cuda_handle, size, device.id()};

            // just return the device ptr, size and device id. owning = false indicate that the handle is not responsible for unmapping the memory.
            return handle(object, /* owning = */ false);
        })->second.data;
    }

    view_b manager::at(emu::cstring_view name) {
        auto it = base::find(name);
        if (it == base::end())
            throw std::runtime_error(fmt::format("No region named {} exists.", name));
        auto& h = it->second;
        return h.data;
    }

    emu::optional<shm_handle> manager::find_handle(const std::byte* ptr) {
        for (auto& [name, handle] : static_cast<base&>(*this)) {
            auto addr = reinterpret_cast<const std::byte*>(handle.data.data());
            auto size = handle.data.size();

            if (addr <= ptr && ptr < addr + size) {
                return shm_handle{name, static_cast<std::size_t>(ptr - addr)};
            }
        }
        return emu::nullopt;
    }


} // namespace sardine::region::cuda
