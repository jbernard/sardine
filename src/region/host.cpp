#include <sardine/region/host.hpp>
#include <sardine/region/host/manager.hpp>
#include <sardine/error.hpp>

#include <boost/lexical_cast.hpp>

namespace sardine::region::host
{

    span_b open(std::string name) {
        return manager::instance().open(name);
    }

    span_b create(std::string name, std::size_t file_size) {
        return manager::instance().create(name, file_size);
    }

    span_b open_or_create(std::string name, std::size_t file_size) {
        return manager::instance().open_or_create(name, file_size);
    }

    optional<memory_handle> find_handle(const std::byte* ptr) {
        return manager::instance().find_handle(ptr);
    }

    optional<url> url_of_bytes(span_cb data) {
        return find_handle(data.data()).map([&](memory_handle handle) {
            return sardine::url(fmt::format("{}://{}/{}/{}", url_scheme, handle.name, handle.offset, data.size()));
        });
    }

    result<span_b> bytes_from_url(url_view url) {
        auto data = open(url.host());

        auto segments = url.segments();

        if (segments.size() != 2)
            return make_unexpected(error::host_url_invalid_path);

        auto seg = segments.begin();

        auto offset = boost::lexical_cast<std::size_t>(*seg);
        if (offset > data.size())
            return make_unexpected(error::host_url_offset_overflow);

        seg++;

        auto size = boost::lexical_cast<std::size_t>(*seg);
        if (offset + size > data.size())
            return make_unexpected(error::host_url_size_overflow);

        return data.subspan(offset, size);
    }

} // namespace sardine::region::host
