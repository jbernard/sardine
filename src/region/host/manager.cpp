#include <sardine/region/host/manager.hpp>

#include <sardine/utility.hpp>

#include <fmt/format.h>

namespace sardine::region::host
{

    /// Return the key for the object manager based on the name of the region.
    // inline std::string object_key(const std::string &name) {
    //     return fmt::format("host.{}", name);
    // }

    manager &manager::instance()
    {
        static manager instance;
        return instance;
    }

    span_b manager::open(std::string name) {
        auto& h = find_or_emplace(static_cast<base&>(*this), name, [s = name.c_str()] {
            auto res = boost::interprocess::shared_memory_object(boost::interprocess::open_only, s, boost::interprocess::read_write);
            return handle(res, boost::interprocess::read_write);
        })->second;

        return map(h);
    }

    span_b manager::create(std::string name, std::size_t size) {
        auto& h = emplace_or_throw(static_cast<base&>(*this), name, [s = name.c_str(), size] {
            auto res = boost::interprocess::shared_memory_object(boost::interprocess::create_only, s, boost::interprocess::read_write);
            res.truncate(size);
            return handle(res, boost::interprocess::read_write);
        })->second;

        return map(h);
    }

    span_b manager::open_or_create(std::string name, std::size_t size) {
        auto& h = find_or_emplace(static_cast<base&>(*this), name, [s = name.c_str(), size] {
            auto res = boost::interprocess::shared_memory_object(boost::interprocess::open_or_create, s, boost::interprocess::read_write);
            res.truncate(size);
            return handle(res, boost::interprocess::read_write);
        })->second;

        return map(h);
    }

    span_b manager::at(emu::cstring_view name) {
        auto it = base::find(name);
        if (it == base::end())
            throw std::runtime_error(fmt::format("No region named {} exists.", name));
        auto& h = it->second;
        return map(h);
    }

    emu::optional<memory_handle> manager::find_handle(const std::byte* ptr) {
        for (auto& [name, h] : static_cast<base&>(*this)) {

            auto addr = reinterpret_cast<const std::byte*>(h.get_address());
            auto size = h.get_size();

            if (addr <= ptr && ptr < addr + size) {
                return memory_handle{name, static_cast<std::size_t>(ptr - addr)};
            }
        }
        return emu::nullopt;
    }


} // namespace sardine::region::host
