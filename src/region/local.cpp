#include <sardine/region/local.hpp>

#include <boost/interprocess/detail/os_thread_functions.hpp>

namespace sardine::detail
{

    optional<url> url_of(span_cb data) {
        auto pid = boost::interprocess::ipcdetail::get_current_process_id();

        return sardine::url(fmt::format("{}://{}/{}/{}", url_scheme, pid, data.data(), data.size()));
    }


} // namespace sardine::detail
