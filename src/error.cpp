#include <sardine/error.hpp>

namespace sardine
{

    std::error_category const& sardine_category() {
        static const error_category instance;
        return instance;
    }

    const char * error_category::name() const noexcept {
        return "sardine";
    }

    std::string error_category::message( int ev ) const {
        return "unkown error";
    }

} // namespace sardine
