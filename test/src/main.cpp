#include <gtest/gtest.h>

#include <config.hpp>

#include <boost/interprocess/shared_memory_object.hpp>

struct Environment : ::testing::Environment
{

    // Override this to define how to set up the environment.
    void SetUp() override
    {
        boost::interprocess::shared_memory_object::remove(managed_filename);
        boost::interprocess::shared_memory_object::remove(managed_filename_2);
        boost::interprocess::shared_memory_object::remove(shm_filename);
    }

    // Override this to define how to tear down the environment.
    void TearDown() override
    {
        boost::interprocess::shared_memory_object::remove(managed_filename);
        boost::interprocess::shared_memory_object::remove(managed_filename_2);
        boost::interprocess::shared_memory_object::remove(shm_filename);
    }
};


int main(int argc, char* argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    // gtest takes ownership of the TestEnvironment ptr - we don't delete it.
    ::testing::AddGlobalTestEnvironment(new ::Environment);
    return RUN_ALL_TESTS();
}
