#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <config.hpp>

#include <sardine/url.hpp>

#include <sardine/buffer.hpp>

using namespace sardine;

namespace
{

    TEST(ProducerConsumer, ManagedIntegerProCon)
    {
        auto managed = managed::open_or_create(managed_filename, managed_filesize);

        int& scalar = managed.force_create<int>("integer_procon", 42);

        ASSERT_EQ(scalar, 42);

        auto url = url_of(scalar).value();

        ASSERT_EQ(url.scheme(), managed::url_scheme);
        ASSERT_EQ(url.host(), managed_filename);
        ASSERT_EQ(url.path(), "/integer_procon/4");

        auto p_res = producer<int, host_ctx>::open(url);
        auto c_res = consumer<int, host_ctx>::open(url);

        ASSERT_TRUE(p_res);
        ASSERT_TRUE(c_res);

        auto p = p_res.value();
        auto c = c_res.value();

        ASSERT_EQ(p.view(), 42);
        ASSERT_EQ(c.view(), 42);

        p.view() = 43;

        ASSERT_EQ(p.view(), 43);
        ASSERT_EQ(c.view(), 43);

        c.view() = 44;

        ASSERT_EQ(p.view(), 44);
        ASSERT_EQ(c.view(), 44);

    }

    TEST(ProducerConsumer, ManagedIntegerBufferProCon)
    {
        auto managed = managed::open_or_create(managed_filename, managed_filesize);

        std::span<int> array = managed.force_create<std::span<int>>("span_buffer", 10);

        ASSERT_EQ(array.size(), 10);

        int i = 0;
        for (auto& e : array) e = i++;

        auto url = url_of(array).value();

        ASSERT_EQ(url.scheme(), managed::url_scheme);
        ASSERT_EQ(url.host(), managed_filename);
        ASSERT_EQ(url.path(), "/span_buffer/40");

        auto parameter = json_t::parse(R"(
            {
                "buffer": {
                    "index" : 0,
                    "size" : 2,
                    "offset": 2,
                    "nb" : 3,
                    "policy" : "next"
                }
            }
        )");

        auto p_res = producer<std::span<int>, host_ctx>::open(url, parameter);
        auto c_res = consumer<std::span<int>, host_ctx>::open(url, parameter);

        ASSERT_TRUE(p_res);
        ASSERT_TRUE(c_res);

        auto p = p_res.value();
        auto c = c_res.value();

        host_ctx ctx;

        {
            auto pv = p.view();
            auto cv = c.view();

            // same as parameter["buffer"]["size"]
            ASSERT_EQ(pv.size(), 2);
            ASSERT_EQ(cv.size(), 2);
            ASSERT_EQ(pv.data() - cv.data(), 2);

        }

    }

} // namespace