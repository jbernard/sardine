#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <config.hpp>

#include <sardine/url.hpp>

#include <sardine/buffer.hpp>

using namespace sardine;

namespace
{

    TEST(Box, ManagedIntegerBox)
    {
        auto managed = managed::open_or_create(managed_filename, managed_filesize);

        int& scalar = managed.force_create<int>("integer_box", 42);

        ASSERT_EQ(scalar, 42);

        auto url = url_of(scalar).value();

        ASSERT_EQ(url.scheme(), managed::url_scheme);
        ASSERT_EQ(url.host(), managed_filename);
        ASSERT_EQ(url.path(), "/integer_box/4");

        auto b = box<int>::open(url).value();

        ASSERT_EQ(b.value, 42);

        scalar = 43;

        ASSERT_EQ(b.value, 42);

        b.recv();

        ASSERT_EQ(b.value, 43);

        b.value = 44;

        ASSERT_EQ(scalar, 43);

        b.send();

        ASSERT_EQ(scalar, 44);
    }

    TEST(Box, JsonIntegerBox)
    {
        auto managed = managed::open_or_create(managed_filename, managed_filesize);

        auto json = managed.force_create<json_t>("json");

        json->insert_or_assign("integer", 42);
        auto scalar = json.at("integer");

        auto url = url_of(scalar).value();

        ASSERT_EQ(url.scheme(), json::url_scheme);
        ASSERT_EQ(url.host(), managed_filename);
        // test path is like `/+XXX/YYY with XXX and YYY integers
        ASSERT_THAT(url.path(), testing::ContainsRegex("/\\+[0-9]+/[0-9]+"));

        auto b = box<int>::open(url).value();

        ASSERT_EQ(b.value, 42);

        scalar = 43;

        ASSERT_EQ(b.value, 42);

        b.recv();

        ASSERT_EQ(b.value, 43);

        b.value = 44;

        ASSERT_EQ(scalar.as<int>(), 43);

        b.send();

        ASSERT_EQ(scalar.as<int>(), 44);
    }

} // namespace