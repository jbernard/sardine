#pragma once

constexpr auto managed_filename = "managed_test";
constexpr std::size_t managed_filesize = 1024*1024;
constexpr auto managed_filename_2 = "managed_test_2";
constexpr auto shm_filename = "shm_test";