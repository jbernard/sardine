cmake_minimum_required(VERSION 3.18)

enable_testing()

find_package(GTest REQUIRED)
include(GoogleTest)

add_executable(sardinetest
    src/main.cpp
    src/managed_test.cpp
    src/box_test.cpp
    src/producer_consumer_test.cpp
)

target_include_directories(sardinetest PRIVATE include)

target_link_libraries(sardinetest PRIVATE GTest::gtest sardine)
gtest_discover_tests(sardinetest)